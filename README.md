#Company Car Search App

Software Requirements
---------------------

In order to build/run need to install the following software
* Android Studio 2.3 + (2.3.0, https://developer.android.com/studio/index.html)

Project Build
-------------

* I use some of variables read from local.properties or env.  
local.properties for my local development env for ci. if env has `USE_ENVIROMENT` key i read from env else from local.properties
* I use different  progress_bar.xml for debug and release for testing purpose.

#### Local Properties
```  
# required
# your api infos
apiUrl=YOUR_API_URL
apiKeyName=API_KEY_NAME
apiKeyValue=API_KEY_VALUE

# optionals
# releaseKeystore variables not exist default debug.keystore values will be set
releaseKeystorePath=/PATH/release.keystore
releaseKeystorePassword=PASSWORD
releaseKeystoreAlias=ALIAS
releaseKeystoreAliasPassword=PASSWORD
# if minSdk exists sets minSdk if not it is default 16
minSdk=23
```

Code Style
----------

* This project use .editorconfig file for indent and tabs.

Code Quality
------------

* This project use [checkstyle](http://checkstyle.sourceforge.net/), [pmd](https://pmd.github.io/), [findbugs](http://findbugs.sourceforge.net/) and [lint](http://tools.android.com/tips/lint). You can find these tools configuration under `quailty` folder.
* Please check your code before push with gradle check task `./gradlew check`
* Run all tests `./gradlew clean test connectedAndroidTest`

Logs
----

* This project use for logging [`Timber`](https://github.com/JakeWharton/timber)
* Don't use Log.e, w, i etc... because I use different log level for debug and release
* If apk is release I show only `info`, `warning`, `error` and `assert`. `debug` and `verbose` level isn't shown
* Use `debug` and `verbose` for <b>sensitive data</b> logs such as network calls etc...
