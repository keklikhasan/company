# gson

-keepattributes Signature
-keepattributes *Annotation*

-keep class sun.misc.Unsafe { *; }

-keep class com.keklikhasan.company.core.app.model.** { *; }
-keep class com.keklikhasan.company.core.data.model.** { *; }

-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer