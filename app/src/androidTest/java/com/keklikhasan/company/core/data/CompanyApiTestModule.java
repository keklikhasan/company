package com.keklikhasan.company.core.data;

import com.keklikhasan.company.core.data.client.CompanyApi;

import retrofit2.Retrofit;

import static org.mockito.Mockito.mock;

public class CompanyApiTestModule extends CompanyApiModule {

  @Override
  public CompanyApi provideCompanyCarTypeApiService(Retrofit retrofit) {
    return mock(CompanyApi.class);
  }
}
