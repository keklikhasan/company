package com.keklikhasan.company.app.test.search;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;

import com.keklikhasan.company.R;
import com.keklikhasan.company.app.CompanyTestApp;
import com.keklikhasan.company.app.dagger.ApplicationTestComponent;
import com.keklikhasan.company.app.test.BaseTest;
import com.keklikhasan.company.app.test.util.MockMetaDataGenerator;
import com.keklikhasan.company.app.ui.criteria.activity.CriteriaActivity;
import com.keklikhasan.company.app.ui.search.activity.SearchActivity;
import com.keklikhasan.company.core.app.model.CriteriaResult;
import com.keklikhasan.company.core.data.client.CompanyApi;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.keklikhasan.company.app.test.hepler.ToolbarHelper.testToolbarTitle;
import static com.keklikhasan.company.app.test.hepler.ToolbarHelper.testToolbarVisible;
import static org.hamcrest.CoreMatchers.not;

@RunWith(AndroidJUnit4.class)
@SuppressWarnings("PMD.TooManyStaticImports")
public class SearchActivityTest extends BaseTest {

  @Rule public ActivityTestRule<SearchActivity> activityTestRule
      = new ActivityTestRule<>(SearchActivity.class, true, true);
  @Inject CompanyApi api;

  @Before
  public void setup() {
    Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
    UiDevice.getInstance(instrumentation); //for ui automator
    CompanyTestApp app = (CompanyTestApp) instrumentation.getTargetContext()
        .getApplicationContext();
    ApplicationTestComponent component = (ApplicationTestComponent) app.getComponent();
    //inject dagger
    component.inject(this);
  }

  @Override
  protected Activity getActivity() {
    return activityTestRule.getActivity();
  }

  @Test
  public void test() {
    testToolbarVisible(); // check toolbar visible
    testToolbarTitle(getString(R.string.title_search)); // check toolbar title

    onView(withId(R.id.select_criteria))
        .check(matches(isDisplayed()))
        .check(matches(withText(R.string.button_criteria)));
    onView(withId(R.id.criteria))
        .check(matches(not(isDisplayed())));

    Intents.init();
    try {
      Matcher<Intent> expectedIntent = hasComponent(CriteriaActivity.class.getName());
      CriteriaResult result = MockMetaDataGenerator.generateCriteriaResult();
      Intent intent = new Intent();
      intent.putExtra(CriteriaActivity.EXTRA_CRITERIA_RESULT, result);
      intending(expectedIntent).respondWith(new Instrumentation
          .ActivityResult(Activity.RESULT_OK, intent));
      onView(withId(R.id.select_criteria))
          .perform(click());
      onView(withId(R.id.criteria))
          .check(matches(isDisplayed()))
          .check(matches(withText(String
              .format("Manufacturer: %s %nMain Type: %s %nBuilt Date: %s",
                  result.manufacturer.name, result.mainType.name, result.builtDate.name))));
    } finally {
      Intents.release();
    }
  }

}
