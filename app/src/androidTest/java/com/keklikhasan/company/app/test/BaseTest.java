package com.keklikhasan.company.app.test;

import android.app.Activity;
import android.support.annotation.StringRes;
import android.support.test.espresso.matcher.ViewMatchers;
import android.view.WindowManager;

import org.hamcrest.Matchers;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static org.hamcrest.Matchers.not;

@SuppressWarnings("PMD.TooManyStaticImports")
public abstract class BaseTest {

  @SuppressWarnings("checkstyle:needbraces")
  protected void unlockScreen() {
    Activity activity = getActivity();
    activity.runOnUiThread(() -> activity.getWindow()
      .addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON));
  }

  protected void testToastMessage(String message) {
    onView(ViewMatchers.withText(message))
      .inRoot(withDecorView(not(Matchers.is(getActivity().getWindow().getDecorView()))))
      .check(matches(isDisplayed()));
  }

  protected String getString(@StringRes int resId) {
    return getActivity().getString(resId);
  }

  protected String getString(@StringRes int resId, Object... formatArgs) {
    return getActivity().getString(resId, formatArgs);
  }

  protected abstract Activity getActivity();

}
