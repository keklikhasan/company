package com.keklikhasan.company.app.test.criteria;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.annotation.StringRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;

import com.keklikhasan.company.R;
import com.keklikhasan.company.app.CompanyTestApp;
import com.keklikhasan.company.app.dagger.ApplicationTestComponent;
import com.keklikhasan.company.app.test.BaseTest;
import com.keklikhasan.company.app.test.hepler.RecyclerViewInteraction;
import com.keklikhasan.company.app.test.util.MockData;
import com.keklikhasan.company.app.test.util.MockMetaDataGenerator;
import com.keklikhasan.company.app.ui.criteria.activity.CriteriaActivity;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.data.client.CompanyApi;
import com.keklikhasan.company.core.data.model.ItemsResponse;
import com.keklikhasan.company.core.data.model.PagingItemsResponse;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import io.reactivex.subjects.PublishSubject;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.keklikhasan.company.app.test.hepler.ToolbarHelper.testToolbarTitle;
import static com.keklikhasan.company.app.test.hepler.ToolbarHelper.testToolbarVisible;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
@SuppressWarnings("PMD.TooManyStaticImports")
public class CriteriaActivityTest extends BaseTest {

  @Rule public ActivityTestRule<CriteriaActivity> activityTestRule
      = new ActivityTestRule<>(CriteriaActivity.class, true, false);
  @Inject CompanyApi api;

  @Before
  public void setup() {
    Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
    UiDevice.getInstance(instrumentation); //for ui automator
    CompanyTestApp app = (CompanyTestApp) instrumentation.getTargetContext()
        .getApplicationContext();
    ApplicationTestComponent component = (ApplicationTestComponent) app.getComponent();
    //inject dagger
    component.inject(this);
  }

  @Test
  public void test() {
    PublishSubject<PagingItemsResponse> manufacturersResponse = PublishSubject.create();
    MockData manufacturersMock = MockMetaDataGenerator.manufacturerNetworkEndOfData();
    when(api.manufacturers(any(Integer.class), any(Integer.class)))
        .thenReturn(manufacturersResponse);

    activityTestRule.launchActivity(null);
    unlockScreen();

    // Test Manufacturer
    testToolbarVisible(); // check toolbar visible
    testToolbarTitle(getString(R.string.title_manufacturer)); // check toolbar title
    testLoadingLayout(R.string.label_fetching_manufacturers);
    manufacturersResponse.onNext(manufacturersMock.pagingItemsResponse);
    testRecyclerView(manufacturersMock.pagingResponse.response);


    // Test MainType
    Meta manufacturer = manufacturersMock.pagingResponse.response
        .get(new Random().nextInt(manufacturersMock.pagingResponse.response.size()));
    PublishSubject<PagingItemsResponse> mainTypesResponse = PublishSubject.create();
    MockData mainTypesMock = MockMetaDataGenerator.mainTypeNetworkEndOfData();
    when(api.mainTypes(eq(manufacturer.id), any(Integer.class), any(Integer.class)))
        .thenReturn(mainTypesResponse);
    onView(withId(R.id.recycler_view))
        .perform(RecyclerViewActions.actionOnItem(
            hasDescendant(withText(manufacturer.name)), click()));
    testToolbarVisible(); // check toolbar visible
    testToolbarTitle(getString(R.string.title_main_type)); // check toolbar title
    testLoadingLayout(R.string.label_fetching_main_type);
    mainTypesResponse.onNext(mainTypesMock.pagingItemsResponse);
    testRecyclerView(mainTypesMock.pagingResponse.response);


    // Test BuiltDate
    Meta mainType = mainTypesMock.pagingResponse.response
        .get(new Random().nextInt(mainTypesMock.pagingResponse.response.size()));
    PublishSubject<ItemsResponse> builtDatesResponse = PublishSubject.create();
    MockData builtDatesMock = MockMetaDataGenerator.builtDateNetworkResultItems();
    when(api.builtDates(eq(manufacturer.id), eq(mainType.id)))
        .thenReturn(builtDatesResponse);
    onView(withId(R.id.recycler_view))
        .perform(RecyclerViewActions.actionOnItem(
            hasDescendant(withText(mainType.name)), click()));
    testToolbarVisible(); // check toolbar visible
    testToolbarTitle(getString(R.string.title_built_date)); // check toolbar title
    testLoadingLayout(R.string.label_fetching_built_date);
    builtDatesResponse.onNext(builtDatesMock.itemsResponse);
    testRecyclerView(builtDatesMock.items);
  }

  private void testRecyclerView(List<Meta> items) {
    //check image results
    RecyclerViewInteraction.
        <Meta>onRecyclerView(withId(R.id.recycler_view))
        .withItems(items)
        .check((item, view, e) -> {
          matches(hasDescendant(withText(item.name))).check(view, e);
        });
  }

  private void testLoadingLayout(@StringRes int textRes) {
    // check layouts only loading_view should be visible
    onView(withId(R.id.loading_view))
        .check(matches(isDisplayed()));
    onView(withId(R.id.recycler_view))
        .check(matches(not(isDisplayed())));
    onView(withId(R.id.message_layout))
        .check(matches(not(isDisplayed())));
    // check loading_view layout
    onView(withId(R.id.loading_image))
        .check(matches(isDisplayed()));
    onView(withId(R.id.loading_message))
        .check(matches(isDisplayed()))
        .check(matches(withText(textRes)));
  }

  private void testRecyclerViewLayout() {
    // check layouts only list should be visible
    onView(withId(R.id.message_layout))
        .check(matches(not(isDisplayed())));
    onView(withId(R.id.recycler_view))
        .check(matches(isDisplayed()));
    onView(withId(R.id.loading_view))
        .check(matches(not(isDisplayed())));
  }

  @Override
  protected Activity getActivity() {
    return activityTestRule.getActivity();
  }
}
