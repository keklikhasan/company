package com.keklikhasan.company.app.test.util;

import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

final class MockUtil {

  private MockUtil() {
  }

  static <T> Set<T> toSet(T[] array) {
    Set<T> set = new HashSet<T>();
    if (array != null) {
      Collections.addAll(set, array);
    }
    return set;
  }

  static <T> T pick(Set<T> set) {
    if (set == null || set.isEmpty()) {
      return null;
    }
    @SuppressWarnings({"unchecked", "PMD.ClassCastExceptionWithToArray"})
    T selected = pick((T[]) set.toArray());
    set.remove(selected);
    return selected;
  }

  static <T> T pick(T[] array) {
    if (array == null || array.length < 1) {
      return null;
    }
    return array[new Random().nextInt(array.length)];
  }
}
