package com.keklikhasan.company.app;

import com.keklikhasan.company.app.dagger.ApplicationDebugModule;
import com.keklikhasan.company.app.dagger.DaggerApplicationTestComponent;
import com.keklikhasan.company.core.data.ClientDebugModule;
import com.keklikhasan.company.core.data.CompanyApiTestModule;

public class CompanyTestApp extends CompanyDebugApp {

  @Override
  public void buildComponent() {
    component = DaggerApplicationTestComponent.builder()
        .applicationModule(new ApplicationDebugModule(this))
        .companyApiModule(new CompanyApiTestModule())
        .clientModule(new ClientDebugModule())
        .build();
    component.inject(this);
  }

  @Override
  protected void installLeakCanary() {
    // don't run leak canary on test
  }

}
