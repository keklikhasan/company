package com.keklikhasan.company.app.dagger;

import com.keklikhasan.company.app.test.criteria.CriteriaActivityTest;
import com.keklikhasan.company.app.test.search.SearchActivityTest;
import com.keklikhasan.company.core.data.ClientModule;
import com.keklikhasan.company.core.data.CompanyApiModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, ClientModule.class, CompanyApiModule.class})
public interface ApplicationTestComponent extends ApplicationComponent {

  void inject(SearchActivityTest app);

  void inject(CriteriaActivityTest app);

}
