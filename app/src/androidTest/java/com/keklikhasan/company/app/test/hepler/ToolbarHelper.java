package com.keklikhasan.company.app.test.hepler;

import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.Toolbar;

import com.keklikhasan.company.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;

@SuppressWarnings("PMD.TooManyStaticImports")
public final class ToolbarHelper {

  private ToolbarHelper() {
  }

  private static Matcher<Object> withToolbarTitle(
      final Matcher<CharSequence> textMatcher) {
    return new BoundedMatcher<Object, Toolbar>(Toolbar.class) {
      @Override
      public boolean matchesSafely(Toolbar toolbar) {
        return textMatcher.matches(toolbar.getTitle());
      }

      @Override
      public void describeTo(Description description) {
        description.appendText("with toolbar title: ");
        textMatcher.describeTo(description);
      }
    };
  }

  public static ViewInteraction onToolbar() {
    return onView(withId(R.id.toolbar));
  }

  public static ViewInteraction testToolbarVisible() {
    return onToolbar().check(matches(isDisplayed()));
  }

  public static ViewInteraction testToolbarTitle(CharSequence title) {
    return onToolbar().check(matches(withToolbarTitle(is(title))));
  }

  public static ViewInteraction testToolbarNavigationButtonVisible() {
    return onToolbarNavigationButton()
        .check(matches(isDisplayed()));
  }

  public static ViewInteraction onToolbarNavigationButton() {
    return onView(allOf(withClassName(is(AppCompatImageButton.class.getName())),
        withParent(withId(R.id.toolbar))));
  }

}
