package com.keklikhasan.company.app.dagger;

import com.keklikhasan.company.app.CompanyApp;

public class ApplicationDebugModule extends ApplicationModule {

  public ApplicationDebugModule(CompanyApp app) {
    super(app);
  }

  @Override
  public int provideNetworkTimeoutInSeconds() {
    return 60;
  }

}
