package com.keklikhasan.company.app.logging;

import android.util.Log;

import com.facebook.stetho.inspector.console.ConsolePeerManager;
import com.facebook.stetho.inspector.helper.ChromePeerManager;
import com.facebook.stetho.inspector.protocol.module.Console;

import timber.log.Timber;

/**
 * Copy-paste of:
 * https://gist.github.com/AlbertVilaCalvo/ae814bcc61de8205b8967feddd0d4faa
 * Which avoids logging to logcat.
 */
public class DebugLogTree extends Timber.DebugTree {

  @Override
  protected String createStackElementTag(StackTraceElement element) {
    return super.createStackElementTag(element) + " #" + element.getLineNumber();
  }

  @Override
  protected void log(int priority, String tag, String message, Throwable t) {
    super.log(priority, tag, message, t);
    ConsolePeerManager peerManager = ConsolePeerManager.getOrCreateInstance();
    if (peerManager == null) {
      return;
    }
    Console.MessageLevel logLevel;
    if (priority < Log.INFO) {
      logLevel = Console.MessageLevel.DEBUG;
    } else if (priority == Log.INFO) {
      logLevel = Console.MessageLevel.LOG;
    } else if (priority == Log.WARN) {
      logLevel = Console.MessageLevel.WARNING;
    } else {
      logLevel = Console.MessageLevel.ERROR;
    }
    CLogCustom.writeToConsole(logLevel, Console.MessageSource.OTHER, tag + ": " + message);
  }

  private static class CLogCustom {

    static void writeToConsole(
      ChromePeerManager chromePeerManager,
      Console.MessageLevel logLevel,
      Console.MessageSource messageSource,
      String messageText) {

      Console.ConsoleMessage message = new Console.ConsoleMessage();
      message.source = messageSource;
      message.level = logLevel;
      message.text = messageText;
      Console.MessageAddedRequest messageAddedRequest = new Console.MessageAddedRequest();
      messageAddedRequest.message = message;
      chromePeerManager.sendNotificationToPeers("Console.messageAdded", messageAddedRequest);
    }

    static void writeToConsole(
      Console.MessageLevel logLevel,
      Console.MessageSource messageSource,
      String messageText
    ) {
      ConsolePeerManager peerManager = ConsolePeerManager.getInstanceOrNull();
      if (peerManager == null) {
        return;
      }

      writeToConsole(peerManager, logLevel, messageSource, messageText);
    }
  }
}
