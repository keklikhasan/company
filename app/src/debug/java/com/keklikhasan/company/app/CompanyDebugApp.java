
package com.keklikhasan.company.app;

import android.os.StrictMode;

import com.facebook.stetho.Stetho;
import com.keklikhasan.company.app.dagger.ApplicationDebugModule;
import com.keklikhasan.company.app.dagger.DaggerApplicationComponent;
import com.keklikhasan.company.app.logging.DebugLogTree;
import com.keklikhasan.company.core.data.ClientDebugModule;
import com.keklikhasan.company.core.data.CompanyApiModule;
import com.squareup.leakcanary.LeakCanary;

import timber.log.Timber;

public class CompanyDebugApp extends CompanyApp {

  @Override
  public void onCreate() {
    super.onCreate();
    Stetho.initializeWithDefaults(this);
    installLeakCanary();
    enabledStrictMode();
  }

  @Override
  protected void initTimber() {
    Timber.plant(new DebugLogTree());
  }

  protected void installLeakCanary() {
    if (!LeakCanary.isInAnalyzerProcess(this)) {
      LeakCanary.install(this);
    }
  }

  private void enabledStrictMode() {
    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
        .detectAll()
        .penaltyLog()
        .penaltyDeath()
        .build());
  }

  @Override
  protected void buildComponent() {
    component = DaggerApplicationComponent.builder()
        .applicationModule(new ApplicationDebugModule(this))
        .companyApiModule(new CompanyApiModule())
        .clientModule(new ClientDebugModule())
        .build();
    component.inject(this);
  }
}
