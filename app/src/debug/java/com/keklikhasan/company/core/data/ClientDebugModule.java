package com.keklikhasan.company.core.data;

import okhttp3.logging.HttpLoggingInterceptor;

public class ClientDebugModule extends ClientModule {

  @Override
  public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
    logging.setLevel(HttpLoggingInterceptor.Level.BODY);
    return logging;
  }

}
