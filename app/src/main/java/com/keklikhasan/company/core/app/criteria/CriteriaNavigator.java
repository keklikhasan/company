package com.keklikhasan.company.core.app.criteria;

import com.keklikhasan.company.core.app.model.Meta;

public interface CriteriaNavigator {

  void toMainTypeList(Meta manufacturer);

  void toBuiltDateList(Meta manufacturer, Meta mainType);

  void returnWithValues(Meta manufacturer, Meta mainType, Meta builtDate);

}
