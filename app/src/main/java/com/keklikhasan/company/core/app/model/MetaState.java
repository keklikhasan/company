package com.keklikhasan.company.core.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class MetaState implements Parcelable {

  public static final Creator<MetaState> CREATOR = new Creator<MetaState>() {
    @Override
    public MetaState createFromParcel(Parcel source) {
      return new MetaState(source);
    }

    @Override
    public MetaState[] newArray(int size) {
      return new MetaState[size];
    }
  };

  public List<Meta> list = new ArrayList<>();
  public int page;
  public boolean isEndOfData;
  public Throwable throwable;


  public MetaState() {
  }

  protected MetaState(Parcel in) {
    this.list = in.createTypedArrayList(Meta.CREATOR);
    this.page = in.readInt();
    this.isEndOfData = in.readByte() != 0;
    this.throwable = (Throwable) in.readSerializable();
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeTypedList(this.list);
    dest.writeInt(this.page);
    dest.writeByte(this.isEndOfData ? (byte) 1 : (byte) 0);
    dest.writeSerializable(this.throwable);
  }

}
