package com.keklikhasan.company.core.app.criteria;

import com.keklikhasan.company.core.app.base.BasePresenter;

public interface CriteriaPresenter extends BasePresenter<CriteriaView> {

  void showInitialFragment();

}
