package com.keklikhasan.company.core.app.search;

import com.keklikhasan.company.core.app.base.BaseView;

public interface SearchView extends BaseView {

  void hideCriteriaTextView();

  void showCriteriaTextView(String criteriaText);

  void openCriteriaView();

}
