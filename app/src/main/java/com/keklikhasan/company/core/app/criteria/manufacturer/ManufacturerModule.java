package com.keklikhasan.company.core.app.criteria.manufacturer;

import dagger.Module;
import dagger.Provides;

@Module
public class ManufacturerModule {

  @Provides
  @Manufacturer
  public ManufacturerPresenter providePresenter(ManufacturerPresenterImpl presenter) {
    return presenter;
  }

  @Provides
  @Manufacturer
  public ManufacturerInteractor provideInteractor(ManufacturerInteractorImpl interactor) {
    return interactor;
  }

}
