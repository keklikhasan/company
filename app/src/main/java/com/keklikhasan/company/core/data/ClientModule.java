package com.keklikhasan.company.core.data;

import com.keklikhasan.company.core.data.util.AuthInterceptor;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
public class ClientModule {

  @Provides
  @Singleton
  public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor,
                                          AuthInterceptor authInterceptor,
                                          @Named("networkTimeoutInSeconds") int networkTimeout) {
    return new OkHttpClient.Builder()
        .connectTimeout(networkTimeout, TimeUnit.SECONDS)
        .writeTimeout(networkTimeout, TimeUnit.SECONDS)
        .readTimeout(networkTimeout, TimeUnit.SECONDS)
        .addInterceptor(loggingInterceptor)
        .addInterceptor(authInterceptor)
        .build();
  }

  @Provides
  @Singleton
  public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
    logging.setLevel(HttpLoggingInterceptor.Level.NONE);
    return logging;
  }

}
