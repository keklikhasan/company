package com.keklikhasan.company.core.app.search;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchModule {

  @Provides
  @Search
  public SearchPresenter providePresenter(SearchPresenterImpl presenter) {
    return presenter;
  }

}
