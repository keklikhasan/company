package com.keklikhasan.company.core.app.criteria.builtdate;

import com.keklikhasan.company.core.app.criteria.Criteria;

import dagger.Module;
import dagger.Provides;

@Module
public class BuiltDateModule {

  @Provides
  @Criteria
  public BuiltDatePresenter providePresenter(BuiltDatePresenterImpl presenter) {
    return presenter;
  }

  @Provides
  @Criteria
  public BuiltDateInteractor provideInteractor(BuiltDateInteractorImpl interactor) {
    return interactor;
  }

}
