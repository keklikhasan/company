package com.keklikhasan.company.core.app.search;

import com.keklikhasan.company.core.app.model.CriteriaResult;

import javax.inject.Inject;

public class SearchPresenterImpl implements SearchPresenter {

  private SearchView view;
  private CriteriaResult result;

  @Inject
  public SearchPresenterImpl() {
  }

  @Override
  public void bind(SearchView view) {
    unbind();
    this.view = view;
  }

  @Override
  public void unbind() {
    view = null;
    result = null;
  }

  @Override
  public void init() {
    if (result != null) {
      criteriaResult(result);
    } else {
      view.hideCriteriaTextView();
    }
  }

  @Override
  public void onClickCriteria() {
    view.openCriteriaView();
  }

  @Override
  public void criteriaResult(CriteriaResult result) {
    this.result = result;
    view.showCriteriaTextView(String.format("Manufacturer: %s %nMain Type: %s %nBuilt Date: %s",
        result.manufacturer.name, result.mainType.name, result.builtDate.name));
  }

  public CriteriaResult getCriteriaResult() {
    return result;
  }

  public void setCriteriaResult(CriteriaResult result) {
    this.result = result;
  }
}
