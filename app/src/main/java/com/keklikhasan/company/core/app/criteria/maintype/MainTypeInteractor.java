package com.keklikhasan.company.core.app.criteria.maintype;


import com.keklikhasan.company.core.app.base.BaseInteractor;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.PagingResponse;

import java.util.List;

import io.reactivex.Observable;

public interface MainTypeInteractor extends BaseInteractor {

  Observable<PagingResponse<List<Meta>>> fetchMainTypes(String manufacturer, int page);
}
