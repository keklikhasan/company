package com.keklikhasan.company.core.app.criteria.manufacturer;


import com.keklikhasan.company.core.app.base.BaseInteractor;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.PagingResponse;

import java.util.List;

import io.reactivex.Observable;

public interface ManufacturerInteractor extends BaseInteractor {

  Observable<PagingResponse<List<Meta>>> fetchManufacturers(int page);
}
