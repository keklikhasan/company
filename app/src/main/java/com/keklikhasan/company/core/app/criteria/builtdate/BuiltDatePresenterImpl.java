package com.keklikhasan.company.core.app.criteria.builtdate;

import com.keklikhasan.company.core.app.criteria.CriteriaNavigator;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.MetaState;
import com.keklikhasan.company.core.app.util.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

public class BuiltDatePresenterImpl implements BuiltDatePresenter {

  private static final Object SIGNAL_OBJECT = new Object();
  private SchedulerProvider scheduler;
  private BuiltDateInteractor interactor;
  private CriteriaNavigator navigator;
  private BuiltDateView view;
  private Meta manufacturer;
  private Meta mainType;
  private MetaState state;
  private boolean isFetchingData;
  private PublishSubject<List<Meta>> resultSignal;
  private PublishSubject<Object> fetchSignal = PublishSubject.create();
  // current api search disposable for cancel request
  private Disposable apiDisposable;
  // onNextObservable disposable for unbind
  private Disposable fetchDisposable;

  @Inject
  public BuiltDatePresenterImpl(SchedulerProvider scheduler,
                                BuiltDateInteractor interactor,
                                CriteriaNavigator navigator) {
    this.scheduler = scheduler;
    this.interactor = interactor;
    this.navigator = navigator;
  }

  @Override
  public void bind(BuiltDateView view) {
    unbind();
    this.view = view;
    fetchSignal = PublishSubject.create();
  }

  @Override
  public void setManufacturer(Meta manufacturer) {
    this.manufacturer = manufacturer;
  }

  @Override
  public void setMainType(Meta mainType) {
    this.mainType = mainType;
  }

  @Override
  public void init() {
    if (!state.list.isEmpty()) {
      view.refreshBuiltDateList(state.list);
      view.showBuiltDateList();
    } else if (state.throwable != null) {
      view.showErrorView(state.throwable);
    } else {
      fetchSignal.onNext(SIGNAL_OBJECT);
    }
  }

  @Override
  public void unbind() {
    this.view = null;
    state = new MetaState();
    fetchSignal = null;
    isFetchingData = false;
    finishCurrentRequest();
    if (fetchDisposable != null && !fetchDisposable.isDisposed()) {
      fetchDisposable.dispose();
    }
    resultSignal = null;
    fetchDisposable = null;
  }

  @Override
  public Observable<List<Meta>> builtDatesResult() {
    if (resultSignal != null) {
      return resultSignal;
    }
    resultSignal = PublishSubject.create();
    fetchDisposable = fetchSignal.filter(o -> {
      boolean available = !state.isEndOfData && !isFetchingData;
      if (available) {
        isFetchingData = true;
      }
      return available;
    })
        .observeOn(scheduler.mainThread())
        .subscribe(o -> {
          view.showFetchingBuiltDateList();
          apiDisposable = interactor
              .fetchBuiltDates(manufacturer.id, mainType.id)
              .observeOn(scheduler.mainThread())
              .map(this::handleResponse)
              .filter(builtDates -> !builtDates.isEmpty())
              .subscribe(resultSignal::onNext, this::handleError,
                  this::finishCurrentRequest);
        }, this::handleError);
    return resultSignal;
  }

  private List<Meta> handleResponse(List<Meta> response) {
    Timber.d("Fetched builtDates success with [manufacturer:%s, mainType:%s]",
        manufacturer.name, mainType.name);
    state.isEndOfData = true;
    if (response != null && !response.isEmpty()) {
      view.showBuiltDateList();
    } else {
      view.showEmptyView();
    }
    state.list = response;
    return response;
  }

  private void handleError(Throwable throwable) {
    Timber.e(throwable, "Fetching built dates error");
    state.throwable = throwable;
    view.showErrorView(throwable);
  }

  private void finishCurrentRequest() {
    isFetchingData = false;
    if (apiDisposable != null && !apiDisposable.isDisposed()) {
      apiDisposable.dispose();
    }
    apiDisposable = null;
  }

  @Override
  public void onSelectBuiltDate(Meta builtDate) {
    navigator.returnWithValues(manufacturer, mainType, builtDate);
  }

  @Override
  public MetaState getState() {
    return state;
  }

  @Override
  public void setState(MetaState state) {
    this.state = state;
  }

}
