package com.keklikhasan.company.core.app.criteria;

import com.keklikhasan.company.core.app.model.Meta;

import javax.inject.Inject;

public class CriteriaPresenterImpl implements CriteriaPresenter, CriteriaNavigator {

  private CriteriaView view;

  @Inject
  public CriteriaPresenterImpl() {
  }

  @Override
  public void bind(CriteriaView view) {
    this.view = view;
  }

  @Override
  public void unbind() {
    view = null;
  }

  @Override
  public void showInitialFragment() {
    view.showManufacturerList();
  }

  @Override
  public void toMainTypeList(Meta manufacturer) {
    if (view != null) {
      view.showMainTypeList(manufacturer);
    }
  }

  @Override
  public void toBuiltDateList(Meta manufacturer, Meta mainType) {
    if (view != null) {
      view.showBuiltDateList(manufacturer, mainType);
    }
  }

  @Override
  public void returnWithValues(Meta manufacturer, Meta mainType, Meta builtDate) {
    if (view != null) {
      view.returnWithValues(manufacturer, mainType, builtDate);
    }
  }
}
