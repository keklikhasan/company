package com.keklikhasan.company.core.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class ItemsResponse {

  @SerializedName("wkda") public Map<String, String> items;

}
