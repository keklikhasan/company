package com.keklikhasan.company.core.app.criteria.maintype;

import com.keklikhasan.company.core.app.model.Meta;

import java.util.List;

public interface MainTypeView {

  void showEmptyView();

  void showMainTypeList();

  void showFetchingMainTypeList();

  void setLoading(boolean isLoading);

  void showErrorView(Throwable throwable);

  void showErrorDialog(Throwable throwable);

  void refreshMainTypeList(List<Meta> mainTypes);

}
