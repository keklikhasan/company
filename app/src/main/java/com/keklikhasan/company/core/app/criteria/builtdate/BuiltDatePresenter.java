package com.keklikhasan.company.core.app.criteria.builtdate;

import com.keklikhasan.company.core.app.base.BasePresenter;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.MetaState;

import java.util.List;

import io.reactivex.Observable;

public interface BuiltDatePresenter extends BasePresenter<BuiltDateView> {

  void setManufacturer(Meta manufacturer);

  void setMainType(Meta mainType);

  void init();

  Observable<List<Meta>> builtDatesResult();

  void onSelectBuiltDate(Meta builtDate);

  MetaState getState();

  void setState(MetaState state);

}
