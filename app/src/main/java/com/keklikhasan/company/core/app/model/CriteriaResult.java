package com.keklikhasan.company.core.app.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CriteriaResult implements Parcelable {

  public static final Parcelable.Creator<CriteriaResult> CREATOR
      = new Parcelable.Creator<CriteriaResult>() {
    @Override
    public CriteriaResult createFromParcel(Parcel source) {
      return new CriteriaResult(source);
    }

    @Override
    public CriteriaResult[] newArray(int size) {
      return new CriteriaResult[size];
    }
  };

  public Meta manufacturer;
  public Meta mainType;
  public Meta builtDate;

  public CriteriaResult() {
  }

  public CriteriaResult(Meta manufacturer, Meta mainType, Meta builtDate) {
    this.manufacturer = manufacturer;
    this.mainType = mainType;
    this.builtDate = builtDate;
  }

  protected CriteriaResult(Parcel in) {
    this.manufacturer = in.readParcelable(Meta.class.getClassLoader());
    this.mainType = in.readParcelable(Meta.class.getClassLoader());
    this.builtDate = in.readParcelable(Meta.class.getClassLoader());
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(this.manufacturer, flags);
    dest.writeParcelable(this.mainType, flags);
    dest.writeParcelable(this.builtDate, flags);
  }

}
