package com.keklikhasan.company.core.app.criteria.manufacturer;

import com.keklikhasan.company.core.app.model.Meta;

import java.util.List;

public interface ManufacturerView {

  void showEmptyView();

  void showManufacturerList();

  void showFetchingManufacturerList();

  void setLoading(boolean isLoading);

  void showErrorView(Throwable throwable);

  void showErrorDialog(Throwable throwable);

  void refreshManufacturerList(List<Meta> manufacturers);

}
