package com.keklikhasan.company.core.app.model;

public enum PagingStatus {
  NO_RESULT, NEXT_PAGE, END_OF_DATA
}