package com.keklikhasan.company.core.util;

public final class ObjectUtil {

  private ObjectUtil() {
  }

  public static <T> boolean equals(T object1, T object2) {
    if (object1 == null) {
      return object2 == null;
    }
    return object1.equals(object2);
  }

}
