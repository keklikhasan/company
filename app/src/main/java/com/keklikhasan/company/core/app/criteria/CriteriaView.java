package com.keklikhasan.company.core.app.criteria;

import com.keklikhasan.company.core.app.base.BaseView;
import com.keklikhasan.company.core.app.model.Meta;

public interface CriteriaView extends BaseView {

  void showManufacturerList();

  void showMainTypeList(Meta manufacturer);

  void showBuiltDateList(Meta manufacturer, Meta mainType);

  void returnWithValues(Meta manufacturer, Meta mainType, Meta builtDate);

}
