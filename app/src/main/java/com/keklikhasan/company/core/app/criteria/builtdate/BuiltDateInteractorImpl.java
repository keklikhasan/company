package com.keklikhasan.company.core.app.criteria.builtdate;

import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.util.SchedulerProvider;
import com.keklikhasan.company.core.data.client.CompanyApi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import timber.log.Timber;

public class BuiltDateInteractorImpl implements BuiltDateInteractor {

  private CompanyApi companyApi;
  private SchedulerProvider scheduler;

  @Inject
  public BuiltDateInteractorImpl(CompanyApi companyApi, SchedulerProvider scheduler) {
    this.companyApi = companyApi;
    this.scheduler = scheduler;
  }

  @Override
  public Observable<List<Meta>> fetchBuiltDates(String manufacturer,
                                                String mainType) {
    Timber.d("Fetch builtDates with [manufacturer:%s, mainType:%s]",
        manufacturer, mainType);
    return companyApi
        .builtDates(manufacturer, mainType)
        .subscribeOn(scheduler.networkThread())
        .map(response -> {
          List<Meta> builtDates = new ArrayList<>();
          if (response.items != null && !response.items.isEmpty()) {
            for (Map.Entry<String, String> entry : response.items.entrySet()) {
              builtDates.add(new Meta(entry.getKey(), entry.getValue()));
            }
            Collections.sort(builtDates);
          }
          return builtDates;
        });
  }

  @Override
  public void unbind() {
  }
}
