package com.keklikhasan.company.core.app.model;

import com.keklikhasan.company.core.util.ObjectUtil;

public class PagingResponse<T> {
  public PagingStatus status;
  public T response;

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof PagingResponse) {
      PagingResponse other = (PagingResponse) obj;
      return ObjectUtil.equals(other.status, status) && ObjectUtil.equals(other.response, response);
    }
    return false;
  }

  @Override
  public int hashCode() {
    int h = 1;
    if (status != null) {
      h *= 1000003;
      h ^= status.hashCode();
    }
    if (response != null) {
      h *= 1000003;
      h ^= response.hashCode();
    }
    return h;
  }

}
