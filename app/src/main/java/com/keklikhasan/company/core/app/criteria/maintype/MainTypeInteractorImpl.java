package com.keklikhasan.company.core.app.criteria.maintype;

import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.PagingResponse;
import com.keklikhasan.company.core.app.model.PagingStatus;
import com.keklikhasan.company.core.app.util.SchedulerProvider;
import com.keklikhasan.company.core.data.client.CompanyApi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import timber.log.Timber;

public class MainTypeInteractorImpl implements MainTypeInteractor {

  private CompanyApi companyApi;
  private SchedulerProvider scheduler;
  private int pageSize;

  @Inject
  public MainTypeInteractorImpl(CompanyApi companyApi,
                                SchedulerProvider scheduler,
                                @Named("pageSize") int pageSize) {
    this.companyApi = companyApi;
    this.scheduler = scheduler;
    this.pageSize = pageSize;
  }

  @Override
  public Observable<PagingResponse<List<Meta>>> fetchMainTypes(String manufacturer, int page) {
    Timber.d("Fetch mainTypes with [manufacturer:%s, page:%s, pageCount:%s]",
        manufacturer, page, pageSize);
    return companyApi
        .mainTypes(manufacturer, page, pageSize)
        .subscribeOn(scheduler.networkThread())
        .map(response -> {
          PagingResponse<List<Meta>> pagingResponse = new PagingResponse<>();
          if (response.items == null || response.items.isEmpty()) {
            if (response.page == 0) {
              // page 0 and items is empty than there is no response
              pagingResponse.status = PagingStatus.NO_RESULT;
            } else {
              // total count is not 0 but data is empty so end of data
              pagingResponse.status = PagingStatus.END_OF_DATA;
            }
            pagingResponse.response = new ArrayList<>();
          } else {
            if (response.page == response.totalPageCount) {
              pagingResponse.status = PagingStatus.END_OF_DATA;
            } else {
              pagingResponse.status = PagingStatus.NEXT_PAGE;
            }
            // Convert the Map<String,String> to Meta object
            List<Meta> mainTypes = new ArrayList<>();
            for (Map.Entry<String, String> entry : response.items.entrySet()) {
              mainTypes.add(new Meta(entry.getKey(), entry.getValue()));
            }
            Collections.sort(mainTypes);
            pagingResponse.response = mainTypes;
          }
          return pagingResponse;
        });
  }

  @Override
  public void unbind() {
  }
}
