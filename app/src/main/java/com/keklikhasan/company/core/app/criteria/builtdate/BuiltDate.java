package com.keklikhasan.company.core.app.criteria.builtdate;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface BuiltDate {
}
