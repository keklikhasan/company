package com.keklikhasan.company.core.app.criteria.manufacturer;

import com.keklikhasan.company.core.app.base.BasePresenter;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.MetaState;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public interface ManufacturerPresenter extends BasePresenter<ManufacturerView> {

  void init();

  Observable<List<Meta>> paginatedResult();

  PublishSubject<Object> getOnNextObservable();

  void onSelectManufacturer(Meta manufacturer);

  MetaState getState();

  void setState(MetaState state);

}
