package com.keklikhasan.company.core.app.criteria.builtdate;

import com.keklikhasan.company.core.app.base.BaseInteractor;
import com.keklikhasan.company.core.app.model.Meta;

import java.util.List;

import io.reactivex.Observable;

public interface BuiltDateInteractor extends BaseInteractor {

  Observable<List<Meta>> fetchBuiltDates(String manufacturer, String mainType);
}
