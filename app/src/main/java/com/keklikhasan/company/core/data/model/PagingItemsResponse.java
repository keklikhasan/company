package com.keklikhasan.company.core.data.model;

import com.google.gson.annotations.SerializedName;

public class PagingItemsResponse extends ItemsResponse {

  @SerializedName("page") public int page;
  @SerializedName("pageSize") public int pageSize;
  @SerializedName("totalPageCount") public int totalPageCount;

}
