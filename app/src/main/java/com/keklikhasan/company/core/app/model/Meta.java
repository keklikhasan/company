package com.keklikhasan.company.core.app.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.keklikhasan.company.core.util.ObjectUtil;

public class Meta implements Comparable<Meta>, Parcelable {

  public static final Parcelable.Creator<Meta> CREATOR = new Parcelable.Creator<Meta>() {
    @Override
    public Meta createFromParcel(Parcel source) {
      return new Meta(source);
    }

    @Override
    public Meta[] newArray(int size) {
      return new Meta[size];
    }
  };
  public String id;
  public String name;

  public Meta() {
  }

  public Meta(String id, String name) {
    this.id = id;
    this.name = name;
  }

  protected Meta(Parcel in) {
    this.id = in.readString();
    this.name = in.readString();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Meta) {
      Meta other = (Meta) obj;
      return ObjectUtil.equals(other.id, id) && ObjectUtil.equals(other.name, name);
    }
    return false;
  }

  @Override
  public int hashCode() {
    int h = 1;
    if (id != null) {
      h *= 1000003;
      h ^= id.hashCode();
    }
    if (name != null) {
      h *= 1000003;
      h ^= name.hashCode();
    }
    return h;
  }

  @Override
  public int compareTo(@NonNull Meta o) {
    if (ObjectUtil.equals(name, o.name)) {
      return id.compareTo(o.id);
    }
    return name.compareTo(o.name);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.id);
    dest.writeString(this.name);
  }
}
