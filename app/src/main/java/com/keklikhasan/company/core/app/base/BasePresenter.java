package com.keklikhasan.company.core.app.base;

public interface BasePresenter<T> {

  void bind(T view);

  void unbind();

}
