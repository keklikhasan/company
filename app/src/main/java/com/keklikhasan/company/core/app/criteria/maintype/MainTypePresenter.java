package com.keklikhasan.company.core.app.criteria.maintype;

import com.keklikhasan.company.core.app.base.BasePresenter;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.MetaState;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public interface MainTypePresenter extends BasePresenter<MainTypeView> {

  void setManufacturer(Meta manufacturer);

  void init();

  Observable<List<Meta>> paginatedResult();

  PublishSubject<Object> getOnNextObservable();

  void onSelectMainType(Meta mainType);

  MetaState getState();

  void setState(MetaState state);

}
