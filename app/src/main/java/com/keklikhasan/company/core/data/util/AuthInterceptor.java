package com.keklikhasan.company.core.data.util;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Named;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {

  private String apiKeyName;
  private String apiKeyValue;

  @Inject
  public AuthInterceptor(@Named("apiKeyName") String apiKeyName,
                         @Named("apiKeyValue") String apiKeyValue) {
    this.apiKeyName = apiKeyName;
    this.apiKeyValue = apiKeyValue;
  }

  @Override
  public Response intercept(Chain chain) throws IOException {
    Request request = chain.request();
    HttpUrl url = request.url().newBuilder().addQueryParameter(apiKeyName, apiKeyValue).build();
    request = request.newBuilder().url(url).build();
    return chain.proceed(request);
  }

}
