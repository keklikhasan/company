package com.keklikhasan.company.core.app.util;

import io.reactivex.Scheduler;

public interface SchedulerProvider {

  Scheduler mainThread();

  Scheduler networkThread();

}