package com.keklikhasan.company.core.data.client;

import com.keklikhasan.company.core.data.model.ItemsResponse;
import com.keklikhasan.company.core.data.model.PagingItemsResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CompanyApi {

  @GET("v1/car-types/manufacturer")
  Observable<PagingItemsResponse> manufacturers(@Query("page") int page,
                                                @Query("pageSize") int pageSize);

  @GET("v1/car-types/main-types")
  Observable<PagingItemsResponse> mainTypes(@Query("manufacturer") String manufacturer,
                                            @Query("page") int page,
                                            @Query("pageSize") int pageSize);

  @GET("v1/car-types/built-dates")
  Observable<ItemsResponse> builtDates(@Query("manufacturer") String manufacturer,
                                       @Query("main-type") String mainType);

}
