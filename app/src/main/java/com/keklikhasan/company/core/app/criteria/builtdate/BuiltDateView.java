package com.keklikhasan.company.core.app.criteria.builtdate;

import com.keklikhasan.company.core.app.model.Meta;

import java.util.List;

public interface BuiltDateView {

  void showEmptyView();

  void showBuiltDateList();

  void showFetchingBuiltDateList();

  void showErrorView(Throwable throwable);

  void refreshBuiltDateList(List<Meta> builtDates);

}
