package com.keklikhasan.company.core.app.criteria.maintype;

import dagger.Module;
import dagger.Provides;

@Module
public class MainTypeModule {

  @Provides
  @MainType
  public MainTypePresenter providePresenter(MainTypePresenterImpl presenter) {
    return presenter;
  }

  @Provides
  @MainType
  public MainTypeInteractor provideInteractor(MainTypeInteractorImpl interactor) {
    return interactor;
  }

}
