package com.keklikhasan.company.core.app.search;

import com.keklikhasan.company.core.app.base.BasePresenter;
import com.keklikhasan.company.core.app.model.CriteriaResult;

public interface SearchPresenter extends BasePresenter<SearchView> {

  void init();

  void onClickCriteria();

  void criteriaResult(CriteriaResult result);

  CriteriaResult getCriteriaResult();

  void setCriteriaResult(CriteriaResult result);

}
