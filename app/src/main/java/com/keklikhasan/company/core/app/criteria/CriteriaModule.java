package com.keklikhasan.company.core.app.criteria;

import dagger.Module;
import dagger.Provides;

@Module
public class CriteriaModule {

  private CriteriaPresenterImpl navigator;

  @Provides
  @Criteria
  public CriteriaNavigator provideNavigator(CriteriaPresenter presenter) {
    return navigator;
  }

  @Provides
  @Criteria
  public CriteriaPresenter providePresenter(CriteriaPresenterImpl presenter) {
    this.navigator = presenter;
    return presenter;
  }

}
