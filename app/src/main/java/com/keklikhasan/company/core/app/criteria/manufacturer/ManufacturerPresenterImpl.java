package com.keklikhasan.company.core.app.criteria.manufacturer;

import com.keklikhasan.company.core.app.criteria.CriteriaNavigator;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.MetaState;
import com.keklikhasan.company.core.app.model.PagingResponse;
import com.keklikhasan.company.core.app.model.PagingStatus;
import com.keklikhasan.company.core.app.util.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

public class ManufacturerPresenterImpl implements ManufacturerPresenter {

  private static final Object SIGNAL_OBJECT = new Object();
  private SchedulerProvider scheduler;
  private ManufacturerInteractor interactor;
  private CriteriaNavigator navigator;
  private ManufacturerView view;

  private PublishSubject<List<Meta>> paginatedResultSignal;
  private PublishSubject<Object> onNextSignal = PublishSubject.create();

  private MetaState state;
  private boolean isFetchingData;

  // current api search disposable for cancel request
  private Disposable apiDisposable;
  // onNextObservable disposable for unbind
  private Disposable onNextDisposable;

  @Inject
  public ManufacturerPresenterImpl(SchedulerProvider scheduler,
                                   ManufacturerInteractor interactor,
                                   CriteriaNavigator navigator) {
    this.scheduler = scheduler;
    this.interactor = interactor;
    this.navigator = navigator;
  }

  @Override
  public void bind(ManufacturerView view) {
    unbind();
    this.view = view;
    onNextSignal = PublishSubject.create();
  }

  @Override
  public void unbind() {
    this.view = null;
    state = new MetaState();
    onNextSignal = null;
    finishCurrentRequest();
    clearPaging();
    stopPaging();
  }

  private void clearPaging() {
    state.page = 0;
    state.isEndOfData = false;
    isFetchingData = false;
  }

  private void stopPaging() {
    paginatedResultSignal = null;
    if (onNextDisposable != null && !onNextDisposable.isDisposed()) {
      onNextDisposable.dispose();
    }
    onNextDisposable = null;
  }

  @Override
  public void init() {
    if (state.list.isEmpty()) {
      if (state.throwable != null) {
        handleError(state.throwable);
      } else if (!state.isEndOfData) {
        onNextSignal.onNext(SIGNAL_OBJECT);
      } else {
        view.showEmptyView();
      }
    } else {
      view.refreshManufacturerList(state.list);
      view.showManufacturerList();
      view.setLoading(!state.isEndOfData);
    }
  }

  @Override
  public PublishSubject<Object> getOnNextObservable() {
    return onNextSignal;
  }

  @Override
  public Observable<List<Meta>> paginatedResult() {
    if (paginatedResultSignal != null) {
      return paginatedResultSignal;
    }
    paginatedResultSignal = PublishSubject.create();
    onNextDisposable = onNextSignal.filter(o -> {
      boolean available = !state.isEndOfData && !isFetchingData;
      if (available) {
        isFetchingData = true;
      }
      return available;
    })
        .observeOn(scheduler.mainThread())
        .subscribe(o -> {
          checkIfIsFirstPage();
          apiDisposable = interactor
              .fetchManufacturers(state.page)
              .observeOn(scheduler.mainThread())
              .map(this::handleResponse)
              .filter(manufacturers -> !manufacturers.isEmpty())
              .subscribe(paginatedResultSignal::onNext, this::handleError,
                  this::finishCurrentRequest);
        }, this::handleError);
    return paginatedResultSignal;
  }

  private void finishCurrentRequest() {
    isFetchingData = false;
    if (apiDisposable != null && !apiDisposable.isDisposed()) {
      apiDisposable.dispose();
    }
    apiDisposable = null;
  }

  private void checkIfIsFirstPage() {
    if (state.page == 0) {
      view.showFetchingManufacturerList();
    }
  }

  private List<Meta> handleResponse(PagingResponse<List<Meta>> response) {
    Timber.d("Fetched manufacturers success with [page:%s]", state.page);
    // check if is data end
    state.isEndOfData = (response.status == PagingStatus.END_OF_DATA
        || response.status == PagingStatus.NO_RESULT);
    switch (response.status) {
      case NO_RESULT:
        // show empty view if there is no result to show
        Timber.d("Fetched manufacturers NO_RESULT");
        view.showEmptyView();
        break;
      case NEXT_PAGE:
        Timber.d("Fetched manufacturers NEXT_PAGE, length : %s", response.response.size());
        view.showManufacturerList();
        state.page++;
        break;
      case END_OF_DATA:
        Timber.d("Fetched manufacturers END_OF_DATA, length : %s", response.response.size());
        view.showManufacturerList();
        break;
      default:
        break;
    }
    view.setLoading(!state.isEndOfData);
    state.list.addAll(response.response);
    return response.response;
  }

  private void handleError(Throwable throwable) {
    Timber.e(throwable, "Fetching manufacturers error");
    state.isEndOfData = true;
    view.setLoading(false);
    if (state.page == 0) {
      state.throwable = throwable;
      view.showErrorView(throwable);
    } else {
      view.showErrorDialog(throwable);
    }
  }

  @Override
  public void onSelectManufacturer(Meta manufacturer) {
    navigator.toMainTypeList(manufacturer);
  }

  @Override
  public MetaState getState() {
    return state;
  }

  @Override
  public void setState(MetaState state) {
    this.state = state;
  }

}
