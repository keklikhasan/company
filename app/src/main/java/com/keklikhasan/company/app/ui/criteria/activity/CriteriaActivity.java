package com.keklikhasan.company.app.ui.criteria.activity;

import android.content.Intent;

import com.keklikhasan.company.R;
import com.keklikhasan.company.app.CompanyApp;
import com.keklikhasan.company.app.ui.base.BaseActivity;
import com.keklikhasan.company.app.ui.criteria.fragment.BuiltDateFragment;
import com.keklikhasan.company.app.ui.criteria.fragment.MainTypeFragment;
import com.keklikhasan.company.app.ui.criteria.fragment.ManufacturerFragment;
import com.keklikhasan.company.core.app.criteria.CriteriaPresenter;
import com.keklikhasan.company.core.app.criteria.CriteriaView;
import com.keklikhasan.company.core.app.model.CriteriaResult;
import com.keklikhasan.company.core.app.model.Meta;

import javax.inject.Inject;

public class CriteriaActivity extends BaseActivity implements CriteriaView {

  public static final String EXTRA_CRITERIA_RESULT = "criteria_result";
  @Inject protected CriteriaPresenter presenter;

  @Override
  protected int getLayoutResId() {
    return R.layout.actrivity_base;
  }

  @Override
  public void injectDependencies(CompanyApp application) {
    application.getSubComponentManager().getCriteriaSubComponent().inject(this);
    presenter.bind(this);
  }

  @Override
  protected void releaseSubComponents(CompanyApp application) {
    application.getSubComponentManager().releaseCarSearchSubComponent();
  }

  @Override
  public void showInitialFragment() {
    presenter.showInitialFragment();
  }

  @Override
  public void showManufacturerList() {
    showFragment(ManufacturerFragment.newInstance(), false);
  }

  @Override
  public void showMainTypeList(Meta manufacturer) {
    showFragment(MainTypeFragment.newInstance(manufacturer), true);
  }

  @Override
  public void showBuiltDateList(Meta manufacturer, Meta mainType) {
    showFragment(BuiltDateFragment.newInstance(manufacturer, mainType), true);
  }


  @Override
  public void returnWithValues(Meta manufacturer, Meta mainType, Meta builtDate) {
    Intent intent = new Intent();
    intent.putExtra(EXTRA_CRITERIA_RESULT, new CriteriaResult(manufacturer, mainType, builtDate));
    setResult(RESULT_OK, intent);
    finish();
  }

  @Override
  protected void onDestroy() {
    presenter.unbind();
    super.onDestroy();
  }

}
