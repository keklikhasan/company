package com.keklikhasan.company.app.ui.criteria;

import com.keklikhasan.company.app.ui.criteria.activity.CriteriaActivity;
import com.keklikhasan.company.app.ui.criteria.fragment.BuiltDateFragment;
import com.keklikhasan.company.app.ui.criteria.fragment.MainTypeFragment;
import com.keklikhasan.company.app.ui.criteria.fragment.ManufacturerFragment;
import com.keklikhasan.company.core.app.criteria.Criteria;
import com.keklikhasan.company.core.app.criteria.CriteriaModule;
import com.keklikhasan.company.core.app.criteria.builtdate.BuiltDate;
import com.keklikhasan.company.core.app.criteria.builtdate.BuiltDateModule;
import com.keklikhasan.company.core.app.criteria.maintype.MainType;
import com.keklikhasan.company.core.app.criteria.maintype.MainTypeModule;
import com.keklikhasan.company.core.app.criteria.manufacturer.Manufacturer;
import com.keklikhasan.company.core.app.criteria.manufacturer.ManufacturerModule;

import dagger.Subcomponent;

@Criteria
@Manufacturer
@MainType
@BuiltDate
@Subcomponent(modules = {CriteriaModule.class, ManufacturerModule.class,
    MainTypeModule.class, BuiltDateModule.class})
public interface CriteriaSubComponent {

  void inject(CriteriaActivity activity);

  void inject(ManufacturerFragment fragment);

  void inject(MainTypeFragment fragment);

  void inject(BuiltDateFragment fragment);
}
