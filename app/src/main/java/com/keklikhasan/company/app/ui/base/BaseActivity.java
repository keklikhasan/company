package com.keklikhasan.company.app.ui.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.keklikhasan.company.R;
import com.keklikhasan.company.app.CompanyApp;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

  @Nullable @BindView(R.id.toolbar) protected Toolbar toolbar;

  @LayoutRes
  protected abstract int getLayoutResId();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(getLayoutResId());
    injectDependencies(CompanyApp.get(this));
    ButterKnife.bind(this);
    if (toolbar != null) {
      setSupportActionBar(toolbar);
    }
    if (savedInstanceState == null) {
      showInitialFragment();
    }
  }

  public void showInitialFragment() {
  }

  public abstract void injectDependencies(CompanyApp application);

  public void showFragment(BaseFragment fragment, boolean addToBackStack) {
    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction ft = fragmentManager.beginTransaction();
    fragment.setCustomAnimations(ft);
    ft.replace(R.id.fragment_container, fragment, fragment.getFragmentTag());
    if (addToBackStack) {
      ft.addToBackStack(fragment.getFragmentTag());
    }
    ft.commit();
  }

  @Override
  protected void onDestroy() {
    releaseSubComponents(CompanyApp.get(this));
    super.onDestroy();
  }

  @Nullable
  public Toolbar getToolbar() {
    return toolbar;
  }

  protected abstract void releaseSubComponents(CompanyApp application);

}
