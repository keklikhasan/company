package com.keklikhasan.company.app.ui.criteria.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class MetaItemCommonViewHolder extends RecyclerView.ViewHolder {

  MetaItemCommonViewHolder(View itemView) {
    super(itemView);
  }

  public abstract void unbind();
}
