package com.keklikhasan.company.app.ui.search;

import com.keklikhasan.company.app.ui.search.activity.SearchActivity;
import com.keklikhasan.company.core.app.search.Search;
import com.keklikhasan.company.core.app.search.SearchModule;

import dagger.Subcomponent;

@Search
@Subcomponent(modules = {SearchModule.class})
public interface SearchSubComponent {

  void inject(SearchActivity activity);
}
