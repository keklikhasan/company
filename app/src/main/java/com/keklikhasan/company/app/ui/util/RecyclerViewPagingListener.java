package com.keklikhasan.company.app.ui.util;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

public class RecyclerViewPagingListener extends RecyclerView.OnScrollListener {

  private static final Object SIGNAL_OBJECT = new Object();
  private int pageSize;
  private LinearLayoutManager layoutManager;
  private PublishSubject<Object> onNextObservable;


  public RecyclerViewPagingListener(LinearLayoutManager layoutManager, int pageSize,
                                    PublishSubject<Object> onNextObservable) {
    this.layoutManager = layoutManager;
    this.pageSize = pageSize;
    this.onNextObservable = onNextObservable;
  }

  @Override
  public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
    if (onNextObservable == null) {
      return;
    }
    int visibleItemCount = layoutManager.getChildCount();
    int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
    int totalItemCount = layoutManager.getItemCount();
    int pastItemCount = visibleItemCount + pastVisibleItems;
    // load next when reaming item counts lower than %33 apiPerPage counts
    if ((totalItemCount - pastItemCount) < (pageSize * 0.33)) {
      Timber.d("Search on scroll %s/%s < %s",
          pastItemCount, totalItemCount, pageSize * 0.33);
      onNextObservable.onNext(SIGNAL_OBJECT);
    }
    super.onScrolled(recyclerView, dx, dy);
  }

  public PublishSubject<Object> getNextObservable() {
    return onNextObservable;
  }

  public void stopPaging() {
    if (onNextObservable != null) {
      onNextObservable.onComplete();
    }
    onNextObservable = null;
  }
}
