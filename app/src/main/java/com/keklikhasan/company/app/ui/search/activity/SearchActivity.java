package com.keklikhasan.company.app.ui.search.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.keklikhasan.company.R;
import com.keklikhasan.company.app.CompanyApp;
import com.keklikhasan.company.app.ui.base.BaseActivity;
import com.keklikhasan.company.app.ui.criteria.activity.CriteriaActivity;
import com.keklikhasan.company.core.app.search.SearchPresenter;
import com.keklikhasan.company.core.app.search.SearchView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class SearchActivity extends BaseActivity implements SearchView {

  private static final String BUNDLE_STATE = "state";
  private static final int REQUEST_CODE = 99;

  @Inject protected SearchPresenter presenter;

  @BindView(R.id.criteria) protected AppCompatTextView criteriaTextView;

  @Override
  protected int getLayoutResId() {
    return R.layout.actrivity_search;
  }

  @Override
  public void injectDependencies(CompanyApp application) {
    application.getSubComponentManager().getSearchSubComponent().inject(this);
    presenter.bind(this);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setupViews();
    if (savedInstanceState != null && savedInstanceState.containsKey(BUNDLE_STATE)) {
      presenter.setCriteriaResult(savedInstanceState.getParcelable(BUNDLE_STATE));
    }
    presenter.init();
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    outState.putParcelable(BUNDLE_STATE, presenter.getCriteriaResult());
    super.onSaveInstanceState(outState);
  }

  private void setupViews() {
    setTitle(R.string.title_search);
  }

  @OnClick(R.id.select_criteria)
  public void onClickCriteria() {
    presenter.onClickCriteria();
  }

  @Override
  protected void releaseSubComponents(CompanyApp application) {
    application.getSubComponentManager().releaseSearchSubComponent();
  }

  @Override
  protected void onDestroy() {
    presenter.unbind();
    super.onDestroy();
  }

  @Override
  public void hideCriteriaTextView() {
    criteriaTextView.setVisibility(View.GONE);
  }

  @Override
  public void showCriteriaTextView(String criteriaText) {
    criteriaTextView.setVisibility(View.VISIBLE);
    criteriaTextView.setText(criteriaText);
  }

  @Override
  public void openCriteriaView() {
    startActivityForResult(new Intent(this, CriteriaActivity.class), REQUEST_CODE);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
      presenter.criteriaResult(data.getParcelableExtra(CriteriaActivity.EXTRA_CRITERIA_RESULT));
    }
    super.onActivityResult(requestCode, resultCode, data);
  }
}
