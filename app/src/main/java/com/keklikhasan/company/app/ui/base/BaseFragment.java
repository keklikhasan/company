package com.keklikhasan.company.app.ui.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.keklikhasan.company.R;
import com.keklikhasan.company.app.CompanyApp;

import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BaseFragment extends Fragment {

  private CompositeDisposable disposables;

  @LayoutRes
  protected abstract int getLayoutResId();

  protected void setCustomAnimations(FragmentTransaction ft) {
    ft.setCustomAnimations(R.anim.slide_in_to_left, R.anim.slide_out_to_left,
        R.anim.slide_in_to_right, R.anim.slide_out_to_right);
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    injectDependencies(CompanyApp.get(context));
    this.disposables = new CompositeDisposable();
    bindPresenter();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View rootView = inflater.inflate(getLayoutResId(), container, false);
    ButterKnife.bind(this, rootView);
    updateToolbar();
    return rootView;
  }

  @Override
  public void onDetach() {
    unbindPresenter();
    this.disposables.dispose();
    super.onDetach();
  }

  protected void setTitle(@StringRes int titleRes) {
    Activity activity = getActivity();
    if (activity != null) {
      activity.setTitle(titleRes);
    }
  }

  private void updateToolbar() {
    Toolbar toolbar = getToolbar();
    if (toolbar != null) {
      if (hasBackNavigationBackIcon()) {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(v -> onClickNavigationBack());
      } else {
        toolbar.setNavigationIcon(null);
        toolbar.setNavigationOnClickListener(null);
      }
    }
  }

  protected boolean hasBackNavigationBackIcon() {
    return false;
  }

  @Nullable
  public Toolbar getToolbar() {
    if (getActivity() != null && getActivity() instanceof BaseActivity) {
      return ((BaseActivity) getActivity()).getToolbar();
    }
    return null;
  }

  public void onClickNavigationBack() {
    simulateBack();
  }

  public void simulateBack() {
    Activity activity = getActivity();
    if (activity != null) {
      activity.onBackPressed();
    }
  }

  protected abstract void injectDependencies(CompanyApp application);

  protected abstract void bindPresenter();

  protected abstract void unbindPresenter();

  public void addDisposable(Disposable disposable) {
    this.disposables.add(disposable);
  }

  public abstract String getFragmentTag();

}
