package com.keklikhasan.company.app.ui.criteria.adapter;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.jakewharton.rxbinding2.view.RxView;
import com.keklikhasan.company.R;
import com.keklikhasan.company.core.app.model.Meta;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

public class MetaItemViewHolder extends MetaItemCommonViewHolder {

  @BindView(R.id.name) AppCompatTextView nameTextView;

  private Observable<Meta> itemClickObservable;
  private Meta meta;

  MetaItemViewHolder(View view) {
    super(view);
    ButterKnife.bind(this, view);
    itemClickObservable = RxView.clicks(view).filter(o -> meta != null).map(o -> meta);
  }

  void bind(Meta meta) {
    this.meta = meta;
    nameTextView.setText(meta.name);
  }

  @Override
  public void unbind() {
    meta = null;
  }

  Observable<Meta> getItemClickObservable() {
    return itemClickObservable;
  }

}
