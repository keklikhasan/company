package com.keklikhasan.company.app.ui.criteria.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.keklikhasan.company.R;
import com.keklikhasan.company.app.CompanyApp;
import com.keklikhasan.company.app.ui.base.BaseFragment;
import com.keklikhasan.company.app.ui.criteria.adapter.MetasRecyclerViewAdapter;
import com.keklikhasan.company.app.ui.util.RecyclerViewPagingListener;
import com.keklikhasan.company.core.app.criteria.manufacturer.ManufacturerPresenter;
import com.keklikhasan.company.core.app.criteria.manufacturer.ManufacturerView;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.util.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import timber.log.Timber;

public class ManufacturerFragment extends BaseFragment implements ManufacturerView {

  private static final String BUNDLE_STATE = "state";
  private static final String BUNDLE_RECYCLER_SCROLL_STATE = "scroll_state";

  @Inject protected Context context;
  @Inject protected ManufacturerPresenter presenter;
  @Inject protected SchedulerProvider scheduler;
  @Inject @Named("pageSize") protected int pageSize;

  @BindView(R.id.recycler_view) protected RecyclerView recyclerView;
  @BindView(R.id.message_layout) protected View messageLayout;
  @BindView(R.id.message) protected AppCompatTextView messageTextView;
  @BindView(R.id.loading_view) protected View loadingLayout;
  @BindView(R.id.loading_message) protected AppCompatTextView loadingMessageTextView;

  private LinearLayoutManager layoutManager;
  private RecyclerViewPagingListener pagingListener;
  private MetasRecyclerViewAdapter recyclerViewAdapter;

  public static ManufacturerFragment newInstance() {
    return new ManufacturerFragment();
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.fragment_meta_list;
  }

  @Override
  protected void injectDependencies(CompanyApp application) {
    application.getSubComponentManager().getCriteriaSubComponent().inject(this);
  }

  @Override
  public String getFragmentTag() {
    return ManufacturerFragment.class.getName();
  }

  @Override
  protected boolean hasBackNavigationBackIcon() {
    return true;
  }

  @Override
  protected void bindPresenter() {
    presenter.bind(this);
    addDisposable(presenter.paginatedResult()
        .subscribeOn(scheduler.mainThread())
        .subscribe(this::refreshManufacturerList, Timber::e));
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    setupViews();
    init(savedInstanceState);
    super.onViewCreated(view, savedInstanceState);
  }

  @Override
  protected void unbindPresenter() {
    presenter.unbind();
    pagingListener.stopPaging();
    recyclerViewAdapter.dispose();
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    outState.putParcelable(BUNDLE_STATE, presenter.getState());
    outState.putInt(BUNDLE_RECYCLER_SCROLL_STATE, layoutManager.findFirstVisibleItemPosition());
    super.onSaveInstanceState(outState);
  }

  public void init(@Nullable Bundle savedInstanceState) {
    int position = -1;
    if (savedInstanceState != null) {
      presenter.setState(savedInstanceState.getParcelable(BUNDLE_STATE));
      position = savedInstanceState.getInt(BUNDLE_RECYCLER_SCROLL_STATE);
    }
    presenter.init();
    if (position > 0) {
      layoutManager.scrollToPosition(position);
    }
  }

  private void setupViews() {
    setTitle(R.string.title_manufacturer);
    recyclerViewAdapter = new MetasRecyclerViewAdapter();
    layoutManager = new LinearLayoutManager(context);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(recyclerViewAdapter);
    pagingListener = new RecyclerViewPagingListener(layoutManager, pageSize,
        presenter.getOnNextObservable());
    recyclerView.addOnScrollListener(pagingListener);
    addDisposable(recyclerViewAdapter.getItemClickObservable()
        .subscribe(this::onClickManufacturer));
  }

  @Override
  public void showEmptyView() {
    messageLayout.setVisibility(View.VISIBLE);
    messageTextView.setText(R.string.label_empty_manufacturer_view);
    recyclerView.setVisibility(View.GONE);
    loadingLayout.setVisibility(View.GONE);
  }

  @Override
  public void showManufacturerList() {
    messageLayout.setVisibility(View.GONE);
    recyclerView.setVisibility(View.VISIBLE);
    loadingLayout.setVisibility(View.GONE);
  }

  @Override
  public void showFetchingManufacturerList() {
    messageLayout.setVisibility(View.GONE);
    recyclerView.setVisibility(View.GONE);
    loadingLayout.setVisibility(View.VISIBLE);
    loadingMessageTextView.setText(R.string.label_fetching_manufacturers);
  }

  @Override
  public void setLoading(boolean isLoading) {
    recyclerViewAdapter.setLoading(isLoading);
    recyclerViewAdapter.notifyItemChanged(recyclerViewAdapter.getMetas().size() + 1);
  }

  @Override
  public void showErrorView(Throwable throwable) {
    messageLayout.setVisibility(View.VISIBLE);
    messageTextView.setText(getErrorMessage(throwable));
    recyclerView.setVisibility(View.GONE);
    loadingLayout.setVisibility(View.GONE);
  }

  @Override
  public void showErrorDialog(Throwable throwable) {
    Toast.makeText(context, getErrorMessage(throwable), Toast.LENGTH_LONG).show();
  }

  @Override
  public void refreshManufacturerList(List<Meta> manufacturers) {
    int size = recyclerViewAdapter.getMetas().size();
    recyclerViewAdapter.getMetas().addAll(manufacturers);
    recyclerViewAdapter.notifyItemRangeInserted(size, manufacturers.size());
  }

  private void onClickManufacturer(Meta manufacturer) {
    presenter.onSelectManufacturer(manufacturer);
  }

  public String getErrorMessage(Throwable throwable) {
    return getString(R.string.error_fetching_manufacturers, throwable.getMessage());
  }

}
