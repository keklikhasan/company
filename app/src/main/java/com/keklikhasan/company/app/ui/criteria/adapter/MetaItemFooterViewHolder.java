package com.keklikhasan.company.app.ui.criteria.adapter;

import android.view.View;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class MetaItemFooterViewHolder extends MetaItemCommonViewHolder {

  private View view;
  private Observable<Boolean> loadingNotify;
  private Disposable disposable;

  MetaItemFooterViewHolder(View view, Observable<Boolean> loadingNotify) {
    super(view);
    this.view = view;
    this.loadingNotify = loadingNotify;
  }

  void bind(boolean loading) {
    view.setVisibility(loading ? View.VISIBLE : View.INVISIBLE);
    disposable = loadingNotify.subscribe(isLoading -> {
      view.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE);
    });
  }

  @Override
  public void unbind() {
    if (disposable != null && disposable.isDisposed()) {
      disposable.dispose();
    }
    disposable = null;
  }

}
