package com.keklikhasan.company.app.dagger;

import android.content.Context;

import com.keklikhasan.company.BuildConfig;
import com.keklikhasan.company.app.CompanyApp;
import com.keklikhasan.company.app.util.ApplicationSchedulerProvider;
import com.keklikhasan.company.app.util.SubComponentManager;
import com.keklikhasan.company.core.app.util.SchedulerProvider;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;

@Module
public class ApplicationModule {

  protected final CompanyApp app;

  public ApplicationModule(CompanyApp app) {
    this.app = app;
  }

  @Provides
  public CompanyApp provideApplication() {
    return app;
  }

  @Provides
  public Context provideContext() {
    return app;
  }

  @Provides
  public SubComponentManager provideSubComponentManager(CompanyApp companyApp) {
    return new SubComponentManager(companyApp.getComponent());
  }

  @Provides
  @Singleton
  public SchedulerProvider provideSchedulerProvider() {
    return new ApplicationSchedulerProvider();
  }

  @Provides
  public HttpUrl provideEndpoint() {
    return HttpUrl.parse(BuildConfig.API_URL);
  }

  @Provides
  @Named("networkTimeoutInSeconds")
  public int provideNetworkTimeoutInSeconds() {
    return 30;
  }

  @Provides
  @Named("pageSize")
  public int providePageSize() {
    return 15;
  }

  @Provides
  @Named("apiKeyName")
  public String provideApiKeyName() {
    return BuildConfig.API_KEY_NAME;
  }

  @Provides
  @Named("apiKeyValue")
  public String provideApiKeyValue() {
    return BuildConfig.API_KEY_VALUE;
  }

}
