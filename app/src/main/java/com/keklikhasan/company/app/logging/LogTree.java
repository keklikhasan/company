package com.keklikhasan.company.app.logging;

import android.util.Log;

import timber.log.Timber;

public class LogTree extends Timber.DebugTree {

  @Override
  protected boolean isLoggable(String tag, int priority) {
    return priority >= Log.INFO;
  }

}
