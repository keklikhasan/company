package com.keklikhasan.company.app.ui.criteria.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.keklikhasan.company.R;
import com.keklikhasan.company.core.app.model.Meta;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;

public class MetasRecyclerViewAdapter extends RecyclerView.Adapter<MetaItemCommonViewHolder> {

  private static final int TYPE_ITEM = 0;
  private static final int TYPE_FOOTER = 1;

  private ArrayList<Meta> metas = new ArrayList<>();
  private PublishSubject<Meta> notify = PublishSubject.create();
  private PublishSubject<Boolean> loadingNotify = PublishSubject.create();
  private boolean isLoading;
  private CompositeDisposable disposables = new CompositeDisposable();

  public MetasRecyclerViewAdapter() {
  }

  public ArrayList<Meta> getMetas() {
    return metas;
  }

  @Override
  public int getItemViewType(int position) {
    if (isFooterItem(position)) {
      return TYPE_FOOTER;
    }
    return TYPE_ITEM;
  }

  @Override
  public MetaItemCommonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    if (viewType == TYPE_ITEM) {
      return new MetaItemViewHolder(LayoutInflater.from(parent.getContext())
          .inflate(R.layout.item_meta, parent, false));
    }
    return new MetaItemFooterViewHolder(LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_meta_footer, parent, false), loadingNotify);
  }

  @Override
  public void onBindViewHolder(final MetaItemCommonViewHolder holder, int position) {
    if (isFooterItem(position)) {
      ((MetaItemFooterViewHolder) holder).bind(isLoading && !metas.isEmpty());
    } else {
      MetaItemViewHolder viewHolder = (MetaItemViewHolder) holder;
      viewHolder.bind(metas.get(position));
      disposables.add(viewHolder.getItemClickObservable().subscribe(notify::onNext));
    }
  }

  @Override
  public void onViewRecycled(MetaItemCommonViewHolder holder) {
    holder.unbind();
    super.onViewRecycled(holder);
  }

  public Observable<Meta> getItemClickObservable() {
    return notify;
  }

  @Override
  public int getItemCount() {
    return metas.size() + 1;
  }

  public void setLoading(boolean isLoading) {
    this.isLoading = isLoading;
    loadingNotify.onNext(this.isLoading);
  }

  private boolean isFooterItem(int position) {
    return metas.isEmpty() || metas.size() <= position;
  }

  public void dispose() {
    if (disposables != null && !disposables.isDisposed()) {
      disposables.dispose();
    }
  }

}
