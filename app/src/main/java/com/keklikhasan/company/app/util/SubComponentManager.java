package com.keklikhasan.company.app.util;

import com.keklikhasan.company.app.dagger.ApplicationComponent;
import com.keklikhasan.company.app.ui.criteria.CriteriaSubComponent;
import com.keklikhasan.company.app.ui.search.SearchSubComponent;
import com.keklikhasan.company.core.app.criteria.CriteriaModule;
import com.keklikhasan.company.core.app.search.SearchModule;

import javax.inject.Inject;

public class SubComponentManager {

  private ApplicationComponent component;
  private CriteriaSubComponent criteriaSubComponent;
  private SearchSubComponent searchSubComponent;

  @Inject
  public SubComponentManager(ApplicationComponent component) {
    this.component = component;
  }

  public CriteriaSubComponent getCriteriaSubComponent() {
    if (criteriaSubComponent == null) {
      criteriaSubComponent = component.plus(new CriteriaModule());
    }
    return criteriaSubComponent;
  }

  public void releaseCarSearchSubComponent() {
    criteriaSubComponent = null;
  }

  public SearchSubComponent getSearchSubComponent() {
    if (searchSubComponent == null) {
      searchSubComponent = component.plus(new SearchModule());
    }
    return searchSubComponent;
  }

  public void releaseSearchSubComponent() {
    criteriaSubComponent = null;
  }

}
