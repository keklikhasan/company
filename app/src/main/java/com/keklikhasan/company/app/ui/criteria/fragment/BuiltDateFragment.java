package com.keklikhasan.company.app.ui.criteria.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.keklikhasan.company.R;
import com.keklikhasan.company.app.CompanyApp;
import com.keklikhasan.company.app.ui.base.BaseFragment;
import com.keklikhasan.company.app.ui.criteria.adapter.MetasRecyclerViewAdapter;
import com.keklikhasan.company.core.app.criteria.builtdate.BuiltDatePresenter;
import com.keklikhasan.company.core.app.criteria.builtdate.BuiltDateView;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.util.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import timber.log.Timber;

public class BuiltDateFragment extends BaseFragment implements BuiltDateView {

  private static final String ARG_MANUFACTURER = "manufacturer";
  private static final String ARG_MAIN_TYPE = "main-type";
  private static final String BUNDLE_STATE = "state";
  private static final String BUNDLE_RECYCLER_SCROLL_STATE = "scroll_state";

  @Inject protected Context context;
  @Inject protected BuiltDatePresenter presenter;
  @Inject protected SchedulerProvider scheduler;
  @Inject @Named("pageSize") protected int pageSize;

  @BindView(R.id.recycler_view) protected RecyclerView recyclerView;
  @BindView(R.id.message_layout) protected View messageLayout;
  @BindView(R.id.message) protected AppCompatTextView messageTextView;
  @BindView(R.id.loading_view) protected View loadingLayout;
  @BindView(R.id.loading_message) protected AppCompatTextView loadingMessageTextView;

  private LinearLayoutManager layoutManager;
  private MetasRecyclerViewAdapter recyclerViewAdapter;

  public static BuiltDateFragment newInstance(Meta manufacturer, Meta mainType) {
    BuiltDateFragment fr = new BuiltDateFragment();
    Bundle bundle = new Bundle();
    bundle.putParcelable(ARG_MANUFACTURER, manufacturer);
    bundle.putParcelable(ARG_MAIN_TYPE, mainType);
    fr.setArguments(bundle);
    return fr;
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.fragment_meta_list;
  }

  @Override
  protected void injectDependencies(CompanyApp application) {
    application.getSubComponentManager().getCriteriaSubComponent().inject(this);
  }

  @Override
  public String getFragmentTag() {
    return BuiltDateFragment.class.getName();
  }

  @Override
  protected boolean hasBackNavigationBackIcon() {
    return true;
  }

  @Override
  protected void bindPresenter() {
    presenter.bind(this);
    setupArguments();
    addDisposable(presenter
        .builtDatesResult()
        .subscribeOn(scheduler.mainThread())
        .subscribe(this::refreshBuiltDateList, Timber::e));
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    setupViews();
    init(savedInstanceState);
    super.onViewCreated(view, savedInstanceState);
  }

  @Override
  protected void unbindPresenter() {
    presenter.unbind();
    recyclerViewAdapter.dispose();
  }

  private void setupArguments() {
    Bundle arguments = getArguments();
    Meta manufacturer = null;
    Meta mainType = null;
    if (arguments != null) {
      if (arguments.containsKey(ARG_MANUFACTURER)) {
        manufacturer = arguments.getParcelable(ARG_MANUFACTURER);
      }
      if (arguments.containsKey(ARG_MAIN_TYPE)) {
        mainType = arguments.getParcelable(ARG_MAIN_TYPE);
      }
    }
    presenter.setManufacturer(manufacturer);
    presenter.setMainType(mainType);
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    outState.putParcelable(BUNDLE_STATE, presenter.getState());
    outState.putInt(BUNDLE_RECYCLER_SCROLL_STATE, layoutManager.findFirstVisibleItemPosition());
    super.onSaveInstanceState(outState);
  }

  public void init(@Nullable Bundle savedInstanceState) {
    int position = -1;
    if (savedInstanceState != null) {
      presenter.setState(savedInstanceState.getParcelable(BUNDLE_STATE));
      position = savedInstanceState.getInt(BUNDLE_RECYCLER_SCROLL_STATE);
    }
    presenter.init();
    if (position > 0) {
      layoutManager.scrollToPosition(position);
    }
  }

  private void setupViews() {
    setTitle(R.string.title_built_date);
    recyclerViewAdapter = new MetasRecyclerViewAdapter();
    recyclerViewAdapter.setLoading(false);
    layoutManager = new LinearLayoutManager(context);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(recyclerViewAdapter);
    addDisposable(recyclerViewAdapter.getItemClickObservable()
        .subscribe(this::onClickBuiltDate));
  }

  @Override
  public void showEmptyView() {
    messageLayout.setVisibility(View.VISIBLE);
    messageTextView.setText(R.string.label_empty_built_date_view);
    recyclerView.setVisibility(View.GONE);
    loadingLayout.setVisibility(View.GONE);
  }

  @Override
  public void showBuiltDateList() {
    messageLayout.setVisibility(View.GONE);
    recyclerView.setVisibility(View.VISIBLE);
    loadingLayout.setVisibility(View.GONE);
  }

  @Override
  public void showFetchingBuiltDateList() {
    messageLayout.setVisibility(View.GONE);
    recyclerView.setVisibility(View.GONE);
    loadingLayout.setVisibility(View.VISIBLE);
    loadingMessageTextView.setText(R.string.label_fetching_built_date);
  }

  @Override
  public void showErrorView(Throwable throwable) {
    messageLayout.setVisibility(View.VISIBLE);
    messageTextView.setText(getErrorMessage(throwable));
    recyclerView.setVisibility(View.GONE);
    loadingLayout.setVisibility(View.GONE);
  }

  @Override
  public void refreshBuiltDateList(List<Meta> builtDates) {
    int size = recyclerViewAdapter.getMetas().size();
    recyclerViewAdapter.getMetas().addAll(builtDates);
    recyclerViewAdapter.notifyItemRangeInserted(size, builtDates.size());
  }

  private void onClickBuiltDate(Meta builtDate) {
    presenter.onSelectBuiltDate(builtDate);
  }

  public String getErrorMessage(Throwable throwable) {
    return getString(R.string.error_fetching_built_dates, throwable.getMessage());
  }

}
