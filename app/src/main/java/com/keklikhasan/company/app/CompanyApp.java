package com.keklikhasan.company.app;

import android.app.Application;
import android.content.Context;

import com.keklikhasan.company.BuildConfig;
import com.keklikhasan.company.app.dagger.ApplicationComponent;
import com.keklikhasan.company.app.dagger.ApplicationModule;
import com.keklikhasan.company.app.dagger.DaggerApplicationComponent;
import com.keklikhasan.company.app.logging.LogTree;
import com.keklikhasan.company.app.util.SubComponentManager;
import com.keklikhasan.company.core.data.ClientModule;
import com.keklikhasan.company.core.data.CompanyApiModule;

import javax.inject.Inject;

import butterknife.ButterKnife;
import timber.log.Timber;

public class CompanyApp extends Application {

  @Inject protected SubComponentManager subComponentManager;
  protected ApplicationComponent component;

  public static CompanyApp get(Context context) {
    return (CompanyApp) context.getApplicationContext();
  }

  @Override
  public void onCreate() {
    super.onCreate();
    initTimber();
    buildComponent();
    initnButterKnife();
  }

  protected void initTimber() {
    Timber.plant(new LogTree());
  }

  protected void initnButterKnife() {
    ButterKnife.setDebug(BuildConfig.DEBUG);
  }

  protected void buildComponent() {
    component = DaggerApplicationComponent.builder()
        .applicationModule(new ApplicationModule(this))
        .companyApiModule(new CompanyApiModule())
        .clientModule(new ClientModule())
        .build();
    component.inject(this);
  }

  public ApplicationComponent getComponent() {
    return component;
  }

  public SubComponentManager getSubComponentManager() {
    return subComponentManager;
  }

}
