package com.keklikhasan.company.app.dagger;

import com.keklikhasan.company.app.CompanyApp;
import com.keklikhasan.company.app.ui.criteria.CriteriaSubComponent;
import com.keklikhasan.company.app.ui.search.SearchSubComponent;
import com.keklikhasan.company.core.app.criteria.CriteriaModule;
import com.keklikhasan.company.core.app.search.SearchModule;
import com.keklikhasan.company.core.data.ClientModule;
import com.keklikhasan.company.core.data.CompanyApiModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, ClientModule.class, CompanyApiModule.class})
public interface ApplicationComponent {

  void inject(CompanyApp companyApp);

  CriteriaSubComponent plus(CriteriaModule module);

  SearchSubComponent plus(SearchModule module);

}
