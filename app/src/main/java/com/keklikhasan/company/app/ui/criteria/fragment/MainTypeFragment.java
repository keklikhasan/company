package com.keklikhasan.company.app.ui.criteria.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.keklikhasan.company.R;
import com.keklikhasan.company.app.CompanyApp;
import com.keklikhasan.company.app.ui.base.BaseFragment;
import com.keklikhasan.company.app.ui.criteria.adapter.MetasRecyclerViewAdapter;
import com.keklikhasan.company.app.ui.util.RecyclerViewPagingListener;
import com.keklikhasan.company.core.app.criteria.maintype.MainTypePresenter;
import com.keklikhasan.company.core.app.criteria.maintype.MainTypeView;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.util.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import timber.log.Timber;

public class MainTypeFragment extends BaseFragment implements MainTypeView {

  private static final String ARG_MANUFACTURER = "manufacturer";
  private static final String BUNDLE_STATE = "state";
  private static final String BUNDLE_RECYCLER_SCROLL_STATE = "scroll_state";

  @Inject protected Context context;
  @Inject protected MainTypePresenter presenter;
  @Inject protected SchedulerProvider scheduler;
  @Inject @Named("pageSize") protected int pageSize;

  @BindView(R.id.recycler_view) protected RecyclerView recyclerView;
  @BindView(R.id.message_layout) protected View messageLayout;
  @BindView(R.id.message) protected AppCompatTextView messageTextView;
  @BindView(R.id.loading_view) protected View loadingLayout;
  @BindView(R.id.loading_message) protected AppCompatTextView loadingMessageTextView;

  private LinearLayoutManager layoutManager;
  private RecyclerViewPagingListener pagingListener;
  private MetasRecyclerViewAdapter recyclerViewAdapter;

  public static MainTypeFragment newInstance(Meta manufacturer) {
    MainTypeFragment fr = new MainTypeFragment();
    Bundle bundle = new Bundle();
    bundle.putParcelable(ARG_MANUFACTURER, manufacturer);
    fr.setArguments(bundle);
    return fr;
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.fragment_meta_list;
  }

  @Override
  protected void injectDependencies(CompanyApp application) {
    application.getSubComponentManager().getCriteriaSubComponent().inject(this);
  }

  @Override
  public String getFragmentTag() {
    return MainTypeFragment.class.getName();
  }

  @Override
  protected boolean hasBackNavigationBackIcon() {
    return true;
  }

  @Override
  protected void bindPresenter() {
    presenter.bind(this);
    setupArguments();
    addDisposable(presenter
        .paginatedResult()
        .subscribeOn(scheduler.mainThread())
        .subscribe(this::refreshMainTypeList, Timber::e));
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    setupViews();
    init(savedInstanceState);
    super.onViewCreated(view, savedInstanceState);
  }

  @Override
  protected void unbindPresenter() {
    presenter.unbind();
    pagingListener.stopPaging();
    recyclerViewAdapter.dispose();
  }

  private void setupArguments() {
    Bundle arguments = getArguments();
    Meta manufacturer = null;
    if (arguments != null && arguments.containsKey(ARG_MANUFACTURER)) {
      manufacturer = arguments.getParcelable(ARG_MANUFACTURER);
    }
    presenter.setManufacturer(manufacturer);
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    outState.putParcelable(BUNDLE_STATE, presenter.getState());
    outState.putInt(BUNDLE_RECYCLER_SCROLL_STATE, layoutManager.findFirstVisibleItemPosition());
    super.onSaveInstanceState(outState);
  }

  public void init(@Nullable Bundle savedInstanceState) {
    int position = -1;
    if (savedInstanceState != null) {
      presenter.setState(savedInstanceState.getParcelable(BUNDLE_STATE));
      position = savedInstanceState.getInt(BUNDLE_RECYCLER_SCROLL_STATE);
    }
    presenter.init();
    if (position > 0) {
      layoutManager.scrollToPosition(position);
    }
  }

  private void setupViews() {
    setTitle(R.string.title_main_type);
    recyclerViewAdapter = new MetasRecyclerViewAdapter();
    layoutManager = new LinearLayoutManager(context);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(recyclerViewAdapter);
    pagingListener = new RecyclerViewPagingListener(layoutManager, pageSize,
        presenter.getOnNextObservable());
    recyclerView.addOnScrollListener(pagingListener);
    addDisposable(recyclerViewAdapter.getItemClickObservable()
        .subscribe(this::onClickMainType));
  }

  @Override
  public void showEmptyView() {
    messageLayout.setVisibility(View.VISIBLE);
    messageTextView.setText(R.string.label_empty_main_type_view);
    recyclerView.setVisibility(View.GONE);
    loadingLayout.setVisibility(View.GONE);
  }

  @Override
  public void showMainTypeList() {
    messageLayout.setVisibility(View.GONE);
    recyclerView.setVisibility(View.VISIBLE);
    loadingLayout.setVisibility(View.GONE);
  }

  @Override
  public void showFetchingMainTypeList() {
    messageLayout.setVisibility(View.GONE);
    recyclerView.setVisibility(View.GONE);
    loadingLayout.setVisibility(View.VISIBLE);
    loadingMessageTextView.setText(R.string.label_fetching_main_type);
  }

  @Override
  public void setLoading(boolean isLoading) {
    recyclerViewAdapter.setLoading(isLoading);
    recyclerViewAdapter.notifyItemChanged(recyclerViewAdapter.getMetas().size() + 1);
  }

  @Override
  public void showErrorView(Throwable throwable) {
    messageLayout.setVisibility(View.VISIBLE);
    messageTextView.setText(getErrorMessage(throwable));
    recyclerView.setVisibility(View.GONE);
    loadingLayout.setVisibility(View.GONE);
  }

  @Override
  public void showErrorDialog(Throwable throwable) {
    Toast.makeText(context, getErrorMessage(throwable), Toast.LENGTH_LONG).show();
  }

  @Override
  public void refreshMainTypeList(List<Meta> mainTypes) {
    int size = recyclerViewAdapter.getMetas().size();
    recyclerViewAdapter.getMetas().addAll(mainTypes);
    recyclerViewAdapter.notifyItemRangeInserted(size, mainTypes.size());
  }

  private void onClickMainType(Meta mainType) {
    presenter.onSelectMainType(mainType);
  }

  public String getErrorMessage(Throwable throwable) {
    return getString(R.string.error_fetching_main_types, throwable.getMessage());
  }

}
