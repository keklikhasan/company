package com.keklikhasan.company.app.util;


import com.keklikhasan.company.core.app.util.SchedulerProvider;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApplicationSchedulerProvider implements SchedulerProvider {

  @Override
  public Scheduler mainThread() {
    return AndroidSchedulers.mainThread();
  }

  @Override
  public Scheduler networkThread() {
    return Schedulers.io();
  }

}