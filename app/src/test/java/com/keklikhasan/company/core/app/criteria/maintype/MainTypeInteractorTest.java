package com.keklikhasan.company.core.app.criteria.maintype;

import com.keklikhasan.company.core.app.criteria.manufacturer.ManufacturerInteractor;
import com.keklikhasan.company.core.app.criteria.manufacturer.ManufacturerInteractorImpl;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.PagingResponse;
import com.keklikhasan.company.core.app.util.SchedulerProvider;
import com.keklikhasan.company.core.data.client.CompanyApi;
import com.keklikhasan.company.test.BaseTest;
import com.keklikhasan.company.test.MockData;
import com.keklikhasan.company.test.MockMetaDataGenerator;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MainTypeInteractorTest extends BaseTest {

  private ManufacturerInteractor interactor;
  private CompanyApi api;

  @Before
  public void setup() {
    api = mock(CompanyApi.class);
    SchedulerProvider scheduler = mock(SchedulerProvider.class);

    // mock scheduler to run immediately
    when(scheduler.mainThread())
        .thenReturn(Schedulers.trampoline());
    when(scheduler.networkThread())
        .thenReturn(Schedulers.trampoline());

    interactor = new ManufacturerInteractorImpl(api, scheduler, 15);
  }


  @Test
  public void testNoResult() throws Exception {
    MockData response = MockMetaDataGenerator.networkNoResult();

    when(api.manufacturers(any(Integer.class), any(Integer.class)))
        .thenReturn(Observable.just(response.pagingItemsResponse));

    TestObserver<PagingResponse<List<Meta>>> testObserver = new TestObserver<>();
    interactor.fetchManufacturers(0).subscribe(testObserver);

    testObserver.assertNoErrors();
    testObserver.assertResult(response.pagingResponse);
  }

  @Test
  public void testEndOfData() throws Exception {
    MockData response = MockMetaDataGenerator.mainTypeNetworkEndOfData();

    when(api.manufacturers(any(Integer.class), any(Integer.class)))
        .thenReturn(Observable.just(response.pagingItemsResponse));

    TestObserver<PagingResponse<List<Meta>>> testObserver = new TestObserver<>();

    interactor.fetchManufacturers(0).subscribe(testObserver);

    testObserver.assertNoErrors();
    testObserver.assertResult(response.pagingResponse);
  }

  @Test
  public void testNextPage() throws Exception {
    MockData response = MockMetaDataGenerator.mainTypeNetworkNextPage();

    when(api.manufacturers(any(Integer.class), any(Integer.class)))
        .thenReturn(Observable.just(response.pagingItemsResponse));

    TestObserver<PagingResponse<List<Meta>>> testObserver = new TestObserver<>();

    interactor.fetchManufacturers(0).subscribe(testObserver);

    testObserver.assertNoErrors();
    testObserver.assertResult(response.pagingResponse);
  }


}
