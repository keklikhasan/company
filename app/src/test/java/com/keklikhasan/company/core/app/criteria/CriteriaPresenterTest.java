package com.keklikhasan.company.core.app.criteria;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CriteriaPresenterTest {

  private CriteriaPresenter presenter;
  private CriteriaView view;

  @Before
  public void setup() {
    view = mock(CriteriaView.class);

    presenter = new CriteriaPresenterImpl();
    presenter.bind(view);
  }

  @Test
  public void testInitialFragment() {
    presenter.showInitialFragment();
    verify(view, times(1)).showManufacturerList();
  }

}
