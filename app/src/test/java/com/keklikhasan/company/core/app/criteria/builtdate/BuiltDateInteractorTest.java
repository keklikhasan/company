package com.keklikhasan.company.core.app.criteria.builtdate;

import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.util.SchedulerProvider;
import com.keklikhasan.company.core.data.client.CompanyApi;
import com.keklikhasan.company.test.BaseTest;
import com.keklikhasan.company.test.MockData;
import com.keklikhasan.company.test.MockMetaDataGenerator;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BuiltDateInteractorTest extends BaseTest {

  private BuiltDateInteractor interactor;
  private CompanyApi api;
  private Meta manufacturer;
  private Meta mainType;

  @Before
  public void setup() {
    api = mock(CompanyApi.class);
    SchedulerProvider scheduler = mock(SchedulerProvider.class);

    // mock scheduler to run immediately
    when(scheduler.mainThread())
        .thenReturn(Schedulers.trampoline());
    when(scheduler.networkThread())
        .thenReturn(Schedulers.trampoline());

    manufacturer = MockMetaDataGenerator.createManufacturer();
    mainType = MockMetaDataGenerator.createMainType();

    interactor = new BuiltDateInteractorImpl(api, scheduler);
  }


  private void testResult(MockData response) throws Exception {
    when(api.builtDates(any(String.class), any(String.class)))
        .thenReturn(Observable.just(response.itemsResponse));

    TestObserver<List<Meta>> testObserver = new TestObserver<>();
    interactor.fetchBuiltDates(manufacturer.id, mainType.id).subscribe(testObserver);

    testObserver.assertNoErrors();
    testObserver.assertResult(response.items);
  }

  @Test
  public void testNoResult() throws Exception {
    testResult(MockMetaDataGenerator.networkNoResultItems());
  }

  @Test
  public void testData() throws Exception {
    testResult(MockMetaDataGenerator.builtDateNetworkResultItems());
  }

}
