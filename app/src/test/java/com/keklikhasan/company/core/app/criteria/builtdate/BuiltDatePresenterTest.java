package com.keklikhasan.company.core.app.criteria.builtdate;

import com.keklikhasan.company.core.app.criteria.CriteriaNavigator;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.util.SchedulerProvider;
import com.keklikhasan.company.test.BaseTest;
import com.keklikhasan.company.test.MockData;
import com.keklikhasan.company.test.MockMetaDataGenerator;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("PMD.TooManyStaticImports")
public class BuiltDatePresenterTest extends BaseTest {

  private BuiltDatePresenter presenter;
  private BuiltDateView view;
  private BuiltDateInteractor interactor;
  private CriteriaNavigator navigator;
  private Meta manufacturer;
  private Meta mainType;

  @Before
  public void setup() {
    SchedulerProvider scheduler = mock(SchedulerProvider.class);
    interactor = mock(BuiltDateInteractor.class);
    view = mock(BuiltDateView.class);
    navigator = mock(CriteriaNavigator.class);

    // mock scheduler to run immediately
    when(scheduler.mainThread())
        .thenReturn(Schedulers.trampoline());
    when(scheduler.networkThread())
        .thenReturn(Schedulers.trampoline());

    manufacturer = MockMetaDataGenerator.createManufacturer();
    mainType = MockMetaDataGenerator.createMainType();

    presenter = new BuiltDatePresenterImpl(scheduler, interactor, navigator);
    presenter.bind(view);
    presenter.setManufacturer(manufacturer);
    presenter.setMainType(mainType);
  }

  @Test
  public void testNoResult() {
    MockData mockData = MockMetaDataGenerator.networkNoResultItems();

    when(interactor.fetchBuiltDates(eq(manufacturer.id), eq(mainType.id)))
        .thenReturn(Observable.just(mockData.items));

    TestObserver<List<Meta>> testObserver = new TestObserver<>();
    presenter.builtDatesResult().subscribe(testObserver);
    presenter.init();
    verify(view, times(1)).showFetchingBuiltDateList();
    verify(view, times(1)).showEmptyView();
    testObserver.assertNoValues();
  }

  @Test
  public void testResult() {
    MockData mockData = MockMetaDataGenerator.builtDateNetworkResultItems();

    when(interactor.fetchBuiltDates(eq(manufacturer.id), eq(mainType.id)))
        .thenReturn(Observable.just(mockData.items));

    TestObserver<List<Meta>> testObserver = new TestObserver<>();
    presenter.builtDatesResult().subscribe(testObserver);
    presenter.init();
    verify(view, times(1)).showFetchingBuiltDateList();
    verify(view, times(1)).showBuiltDateList();
    testObserver.assertValue(mockData.items);
  }

  @Test
  public void testError() {
    PublishSubject<List<Meta>> publishSubject = PublishSubject.create();

    when(interactor.fetchBuiltDates(eq(manufacturer.id), eq(mainType.id)))
        .thenReturn(publishSubject);

    TestObserver<List<Meta>> testObserver = new TestObserver<>();
    presenter.builtDatesResult().subscribe(testObserver);
    presenter.init();

    Throwable throwable = MockMetaDataGenerator.generateThrowable();
    publishSubject.onError(throwable);

    verify(view, times(1)).showFetchingBuiltDateList();
    verify(view, times(1)).showErrorView(throwable);
    testObserver.assertNoValues();
    testObserver.assertNoErrors();
  }

  @Test
  public void testOnSelectBuiltDate() {
    Meta builtDate = MockMetaDataGenerator.createBuiltDate();
    presenter.onSelectBuiltDate(builtDate);
    verify(navigator, times(1)).returnWithValues(manufacturer, mainType, builtDate);
  }

}
