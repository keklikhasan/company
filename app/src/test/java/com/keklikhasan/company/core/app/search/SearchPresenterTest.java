package com.keklikhasan.company.core.app.search;

import com.keklikhasan.company.core.app.model.CriteriaResult;
import com.keklikhasan.company.test.MockMetaDataGenerator;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SearchPresenterTest {

  private SearchPresenter presenter;
  private SearchView view;

  @Before
  public void setup() {
    view = mock(SearchView.class);

    presenter = new SearchPresenterImpl();
    presenter.bind(view);
  }

  @Test
  public void testInit() {
    presenter.init();
    verify(view, times(1)).hideCriteriaTextView();

    CriteriaResult result = MockMetaDataGenerator.generateCriteriaResult();
    presenter.setCriteriaResult(result);
    presenter.init();
    verify(view, times(1))
        .showCriteriaTextView(String.format("Manufacturer: %s %nMain Type: %s %nBuilt Date: %s",
            result.manufacturer.name, result.mainType.name, result.builtDate.name));

    presenter.onClickCriteria();
    verify(view, times(1)).openCriteriaView();
  }

}
