package com.keklikhasan.company.core.util;

import org.junit.Assert;
import org.junit.Test;

public class ObjectUtilTest {

  @Test
  public void testEquals() {
    Assert.assertTrue(ObjectUtil.equals(null, null));
    Assert.assertFalse(ObjectUtil.equals(null, "1"));
    Assert.assertFalse(ObjectUtil.equals("1", null));
    Assert.assertTrue(ObjectUtil.equals("1", "1"));
    Assert.assertTrue(ObjectUtil.equals(1, 1));
  }

}
