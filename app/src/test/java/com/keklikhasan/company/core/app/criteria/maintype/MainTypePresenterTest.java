package com.keklikhasan.company.core.app.criteria.maintype;

import com.keklikhasan.company.core.app.criteria.CriteriaNavigator;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.PagingResponse;
import com.keklikhasan.company.core.app.util.SchedulerProvider;
import com.keklikhasan.company.test.BaseTest;
import com.keklikhasan.company.test.MockMetaDataGenerator;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("PMD.TooManyStaticImports")
public class MainTypePresenterTest extends BaseTest {

  private MainTypePresenter presenter;
  private MainTypeView view;
  private MainTypeInteractor interactor;
  private CriteriaNavigator navigator;
  private Meta manufacturer;

  @Before
  public void setup() {
    SchedulerProvider scheduler = mock(SchedulerProvider.class);
    interactor = mock(MainTypeInteractor.class);
    view = mock(MainTypeView.class);
    navigator = mock(CriteriaNavigator.class);

    // mock scheduler to run immediately
    when(scheduler.mainThread())
        .thenReturn(Schedulers.trampoline());
    when(scheduler.networkThread())
        .thenReturn(Schedulers.trampoline());

    manufacturer = MockMetaDataGenerator.createManufacturer();
    presenter = new MainTypePresenterImpl(scheduler, interactor, navigator);
    presenter.bind(view);
    presenter.setManufacturer(manufacturer);
  }

  @Test
  public void testFetchFirstPageWithNoResult() {
    PagingResponse<List<Meta>> response = MockMetaDataGenerator.noResult();

    when(interactor.fetchMainTypes(eq(manufacturer.id), any(Integer.class)))
        .thenReturn(Observable.just(response));

    TestObserver<List<Meta>> testObserver = new TestObserver<>();
    presenter.paginatedResult().subscribe(testObserver);
    presenter.init();
    verify(view, times(1)).showFetchingMainTypeList();
    verify(view, times(1)).showEmptyView();
    verify(view, times(1)).setLoading(false);
    testObserver.assertNoValues();
  }

  @Test
  public void testSearchResult() {
    PagingResponse<List<Meta>> page0Response = MockMetaDataGenerator.mainTypeNextPage();
    PagingResponse<List<Meta>> page1Response = MockMetaDataGenerator.mainTypeNextPage();
    PagingResponse<List<Meta>> endOfDataResponse = MockMetaDataGenerator.manufacturerEndOfData();

    when(interactor.fetchMainTypes(eq(manufacturer.id), eq(0)))
        .thenReturn(Observable.just(page0Response));
    when(interactor.fetchMainTypes(eq(manufacturer.id), eq(1)))
        .thenReturn(Observable.just(page1Response));
    when(interactor.fetchMainTypes(eq(manufacturer.id), eq(2)))
        .thenReturn(Observable.just(endOfDataResponse));

    TestObserver<List<Meta>> testObserver = new TestObserver<>();
    PublishSubject<Object> onNext = presenter.getOnNextObservable();
    presenter.paginatedResult().subscribe(testObserver);
    presenter.init();
    verify(view, times(1)).showFetchingMainTypeList();
    verify(view, times(1)).showMainTypeList();
    verify(view, times(1)).setLoading(true);
    testObserver.assertNoErrors();
    testObserver.assertValueCount(1);
    testObserver.assertValue(page0Response.response);

    // next page with result
    onNext.onNext(new Object());
    verify(view, times(2)).showMainTypeList();
    verify(view, times(2)).setLoading(true);
    testObserver.assertNoErrors();
    testObserver.assertValueCount(2);
    testObserver.assertValues(page0Response.response, page1Response.response);

    // next page end of data
    onNext.onNext(new Object());
    verify(view, times(3)).showMainTypeList();
    verify(view, times(2)).setLoading(true);
    verify(view, times(1)).setLoading(false);
    testObserver.assertNoErrors();
    testObserver.assertValueCount(3);
    testObserver.assertValues(page0Response.response, page1Response.response,
        endOfDataResponse.response);
  }

  @Test
  public void testErrorOnFirstPage() {
    PublishSubject<PagingResponse<List<Meta>>> publishSubject = PublishSubject.create();

    when(interactor.fetchMainTypes(eq(manufacturer.id), any(Integer.class)))
        .thenReturn(publishSubject);
    Throwable throwable = MockMetaDataGenerator.generateThrowable();
    publishSubject.onError(throwable);

    TestObserver<List<Meta>> testObserver = new TestObserver<>();
    presenter.paginatedResult().subscribe(testObserver);
    presenter.init();
    verify(view, times(1)).setLoading(false);
    verify(view, times(1)).showErrorView(throwable);
  }

  @Test
  public void testError() {
    PagingResponse<List<Meta>> page0Response = MockMetaDataGenerator.mainTypeNextPage();
    PublishSubject<PagingResponse<List<Meta>>> publishSubject = PublishSubject.create();


    when(interactor.fetchMainTypes(eq(manufacturer.id), eq(0)))
        .thenReturn(Observable.just(page0Response));
    when(interactor.fetchMainTypes(eq(manufacturer.id), eq(1)))
        .thenReturn(publishSubject);
    Throwable throwable = MockMetaDataGenerator.generateThrowable();
    publishSubject.onError(throwable);

    TestObserver<List<Meta>> testObserver = new TestObserver<>();
    PublishSubject<Object> onNext = presenter.getOnNextObservable();
    presenter.paginatedResult().subscribe(testObserver);
    presenter.init();
    verify(view, times(1)).showFetchingMainTypeList();
    verify(view, times(1)).showMainTypeList();
    verify(view, times(1)).setLoading(true);
    testObserver.assertNoErrors();
    testObserver.assertValueCount(1);
    testObserver.assertValue(page0Response.response);

    // next page with result
    onNext.onNext(new Object());
    verify(view, times(1)).setLoading(false);
    verify(view, times(1)).showErrorDialog(throwable);
  }

  @Test
  public void testOnSelectMainType() {
    Meta mainType = MockMetaDataGenerator.createMainType();
    presenter.onSelectMainType(mainType);
    verify(navigator, times(1)).toBuiltDateList(manufacturer, mainType);
  }

}
