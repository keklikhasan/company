package com.keklikhasan.company.test;

public class BaseTest {

  protected void sleep(long time) {
    try {
      Thread.sleep(time);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

}
