package com.keklikhasan.company.test;

import com.keklikhasan.company.core.app.model.CriteriaResult;
import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.PagingResponse;
import com.keklikhasan.company.core.app.model.PagingStatus;
import com.keklikhasan.company.core.data.model.ItemsResponse;
import com.keklikhasan.company.core.data.model.PagingItemsResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

public final class MockMetaDataGenerator {

  private static final String[] MANUFACTURER_IDS = {"107", "130", "141", "145", "150", "157", "160",
      "170", "190", "020", "040", "042", "057", "060", "095", "191", "194", "195", "210", "230",
      "277", "280", "285", "315", "340", "345", "357", "362", "365", "380"
  };
  private static final String[] MANUFACTURER_NAMES = {"Bentley", "BMW", "Buick", "Brilliance",
      "Cadillac", "Caterham", "Chevrolet", "Chrysler", "Citroen", "Abarth", "Alfa Romeo",
      "Alpina", "Aston Martin", "Audi", "Barkas", "Corvette", "Dacia", "Daewoo", "Daihatsu",
      "Dodge", "Ferrari", "Fiat", "Ford", "Hummer", "Honda", "Hyundai", "Infiniti", "Isuzu",
      "Iveco", "Jaguar"};
  private static final String[] MAIN_TYPE_IDS = {"B-Max", "C-Max", "Capri", "Cougar",
      "EcoSport", "Econovan", "Edge", "Escort", "Explorer", "Fiesta", "Focus", "Fusion", "Galaxy",
      "Granada", "Grand Tourneo Connect", "Ka", "Ka+", "Kuga", "Maverick", "Mondeo", "Mustang",
      "Orion", "Probe", "Puma", "Ranger", "S-MAX", "Scorpio", "Sierra", "StreetKa", "Taunus"};
  private static final String[] MAIN_TYPE_NAMES = {"B-Max", "C-Max", "Capri", "Cougar",
      "EcoSport", "Econovan", "Edge", "Escort", "Explorer", "Fiesta", "Focus", "Fusion", "Galaxy",
      "Granada", "Grand Tourneo Connect", "Ka", "Ka+", "Kuga", "Maverick", "Mondeo", "Mustang",
      "Orion", "Probe", "Puma", "Ranger", "S-MAX", "Scorpio", "Sierra", "StreetKa", "Taunus"};

  private static final String[] BUILT_DATES = {"1990", "1991", "1992", "1993", "1994", "1995",
      "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005",
      "2006", "2007", "2008", "2009", "2010"};


  private MockMetaDataGenerator() {
  }

  public static Meta createManufacturer() {
    return new Meta(MockUtil.pick(MockUtil.toSet(MANUFACTURER_IDS)),
        MockUtil.pick(MockUtil.toSet(MANUFACTURER_NAMES)));
  }

  public static Meta createMainType() {
    return new Meta(MockUtil.pick(MockUtil.toSet(MAIN_TYPE_IDS)),
        MockUtil.pick(MockUtil.toSet(MAIN_TYPE_NAMES)));
  }

  public static Meta createBuiltDate() {
    return new Meta(MockUtil.pick(MockUtil.toSet(BUILT_DATES)),
        MockUtil.pick(MockUtil.toSet(BUILT_DATES)));
  }

  public static MockData networkNoResult() {
    MockData mockData = new MockData();
    PagingItemsResponse pagingItemsResponse = new PagingItemsResponse();
    pagingItemsResponse.page = 0;
    pagingItemsResponse.totalPageCount = 0;
    pagingItemsResponse.pageSize = 15;
    pagingItemsResponse.items = new HashMap<>();

    PagingResponse<List<Meta>> pagingResponse = new PagingResponse<>();
    pagingResponse.status = PagingStatus.NO_RESULT;
    pagingResponse.response = new ArrayList<>();

    mockData.pagingItemsResponse = pagingItemsResponse;
    mockData.pagingResponse = pagingResponse;
    return mockData;
  }

  public static PagingResponse<List<Meta>> noResult() {
    PagingResponse<List<Meta>> response = new PagingResponse<>();
    response.status = PagingStatus.NO_RESULT;
    response.response = new ArrayList<>();
    return response;
  }

  public static MockData networkNextPage(Set<String> ids, Set<String> names) {
    MockData mockData = new MockData();
    PagingItemsResponse pagingItemsResponse = new PagingItemsResponse();
    pagingItemsResponse.page = 0;
    pagingItemsResponse.totalPageCount = 5;
    pagingItemsResponse.pageSize = 15;
    pagingItemsResponse.items = new HashMap<>();

    PagingResponse<List<Meta>> pagingResponse = new PagingResponse<>();
    pagingResponse.status = PagingStatus.NEXT_PAGE;
    pagingResponse.response = new ArrayList<>();

    int dataLength = pagingItemsResponse.pageSize;
    for (int i = 0; i < dataLength; i++) {
      Meta meta = new Meta(MockUtil.pick(ids), MockUtil.pick(names));
      pagingResponse.response.add(meta);
      pagingItemsResponse.items.put(meta.id, meta.name);
    }
    Collections.sort(pagingResponse.response);

    mockData.pagingItemsResponse = pagingItemsResponse;
    mockData.pagingResponse = pagingResponse;
    return mockData;
  }

  public static MockData manufacturerNetworkNextPage() {
    return networkNextPage(MockUtil.toSet(MANUFACTURER_IDS), MockUtil.toSet(MANUFACTURER_NAMES));
  }

  public static MockData mainTypeNetworkNextPage() {
    return networkNextPage(MockUtil.toSet(MAIN_TYPE_IDS), MockUtil.toSet(MAIN_TYPE_NAMES));
  }

  public static PagingResponse<List<Meta>> nextPage(Set<String> ids, Set<String> names) {
    PagingResponse<List<Meta>> response = new PagingResponse<>();
    response.status = PagingStatus.NEXT_PAGE;
    response.response = new ArrayList<>();

    int dataLength = new Random().nextInt(15) + 1;
    for (int i = 0; i < dataLength; i++) {
      response.response.add(new Meta(MockUtil.pick(ids), MockUtil.pick(names)));
    }
    return response;
  }

  public static PagingResponse<List<Meta>> manufacturerNextPage() {
    return nextPage(MockUtil.toSet(MANUFACTURER_IDS), MockUtil.toSet(MANUFACTURER_NAMES));
  }

  public static PagingResponse<List<Meta>> mainTypeNextPage() {
    return nextPage(MockUtil.toSet(MAIN_TYPE_IDS), MockUtil.toSet(MAIN_TYPE_NAMES));
  }

  private static MockData networkEndOfData(Set<String> ids, Set<String> names) {
    MockData mockData = new MockData();
    PagingItemsResponse pagingItemsResponse = new PagingItemsResponse();
    pagingItemsResponse.page = 0;
    pagingItemsResponse.totalPageCount = 0;
    pagingItemsResponse.pageSize = 15;
    pagingItemsResponse.items = new HashMap<>();

    PagingResponse<List<Meta>> pagingResponse = new PagingResponse<>();
    pagingResponse.status = PagingStatus.END_OF_DATA;
    pagingResponse.response = new ArrayList<>();

    int dataLength = new Random().nextInt(15) + 1;
    for (int i = 0; i < dataLength; i++) {
      Meta meta = new Meta(MockUtil.pick(ids), MockUtil.pick(names));
      pagingResponse.response.add(meta);
      pagingItemsResponse.items.put(meta.id, meta.name);
    }
    Collections.sort(pagingResponse.response);

    mockData.pagingItemsResponse = pagingItemsResponse;
    mockData.pagingResponse = pagingResponse;
    return mockData;
  }

  public static MockData manufacturerNetworkEndOfData() {
    return networkEndOfData(MockUtil.toSet(MANUFACTURER_IDS), MockUtil.toSet(MANUFACTURER_NAMES));
  }

  public static MockData mainTypeNetworkEndOfData() {
    return networkEndOfData(MockUtil.toSet(MAIN_TYPE_IDS), MockUtil.toSet(MAIN_TYPE_NAMES));
  }

  private static PagingResponse<List<Meta>> endOfData(Set<String> ids, Set<String> names) {
    PagingResponse<List<Meta>> response = new PagingResponse<>();
    response.status = PagingStatus.END_OF_DATA;
    response.response = new ArrayList<>();

    int dataLength = new Random().nextInt(15) + 1;
    for (int i = 0; i < dataLength; i++) {
      response.response.add(new Meta(MockUtil.pick(ids), MockUtil.pick(names)));
    }
    return response;
  }

  public static PagingResponse<List<Meta>> manufacturerEndOfData() {
    return endOfData(MockUtil.toSet(MANUFACTURER_IDS), MockUtil.toSet(MANUFACTURER_NAMES));
  }

  public static PagingResponse<List<Meta>> mainTypeEndOfData() {
    return endOfData(MockUtil.toSet(MAIN_TYPE_IDS), MockUtil.toSet(MAIN_TYPE_NAMES));
  }

  public static Throwable generateThrowable() {
    String[] errorMessages = {"Network Error", "500 Server Error", "400 Bad Request"};
    String message = errorMessages[new Random().nextInt(errorMessages.length)];
    int type = new Random().nextInt(3);
    Throwable result = null;
    switch (type) {
      case 0: {
        result = new RuntimeException(message);
        break;
      }
      case 1: {
        result = new Exception(message);
        break;
      }
      case 2: {
        result = new Exception(message);
        break;
      }
      default: {
        break;
      }
    }
    return result;
  }

  public static MockData networkNoResultItems() {
    MockData mockData = new MockData();
    ItemsResponse itemsResponse = new ItemsResponse();
    itemsResponse.items = new HashMap<>();

    mockData.itemsResponse = itemsResponse;
    mockData.items = new ArrayList<>();
    return mockData;
  }

  private static MockData networkResultItems(Set<String> ids, Set<String> names) {
    MockData mockData = new MockData();
    ItemsResponse itemsResponse = new ItemsResponse();
    itemsResponse.items = new HashMap<>();

    mockData.itemsResponse = itemsResponse;
    mockData.items = new ArrayList<>();

    int dataLength = new Random().nextInt(15) + 1;
    for (int i = 0; i < dataLength; i++) {
      Meta meta = new Meta(MockUtil.pick(ids), MockUtil.pick(names));
      mockData.items.add(meta);
      itemsResponse.items.put(meta.id, meta.name);
    }
    Collections.sort(mockData.items);

    return mockData;
  }

  public static MockData builtDateNetworkResultItems() {
    return networkResultItems(MockUtil.toSet(BUILT_DATES), MockUtil.toSet(BUILT_DATES));
  }

  public static CriteriaResult generateCriteriaResult() {
    CriteriaResult result = new CriteriaResult();
    result.manufacturer = createManufacturer();
    result.mainType = createMainType();
    result.builtDate = createBuiltDate();
    return result;
  }

}
