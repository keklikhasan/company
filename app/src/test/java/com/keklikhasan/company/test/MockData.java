package com.keklikhasan.company.test;

import com.keklikhasan.company.core.app.model.Meta;
import com.keklikhasan.company.core.app.model.PagingResponse;
import com.keklikhasan.company.core.data.model.ItemsResponse;
import com.keklikhasan.company.core.data.model.PagingItemsResponse;

import java.util.List;

public class MockData {
  public PagingItemsResponse pagingItemsResponse;
  public PagingResponse<List<Meta>> pagingResponse;

  public ItemsResponse itemsResponse;
  public List<Meta> items;
}
